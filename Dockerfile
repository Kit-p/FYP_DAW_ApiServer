# syntax=docker/dockerfile:1

##
## Build
##
FROM golang:1.17.6-bullseye AS build

# Define env vars
ENV GO111MODULE=on
ENV CGO_ENABLED=0

# Create a directory for the source code
RUN mkdir -p /go/src/app

# Set the working directory for all subsequent commands
WORKDIR /go/src/app

# Copy the module files first and then download the dependencies. If this
# doesn't change, we won't need to do this again in future builds.
COPY go.* ./

RUN go mod download
RUN go mod verify

# Copy all the source code
COPY . ./

# Build the binary
RUN go build -v \
    -a -o /go/bin/app .

##
## Run
##
FROM gcr.io/distroless/static-debian11 AS run

# Copy wget binary from build stage for health checking in Docker Compose
COPY --from=build /go/src/app/bin/wget /usr/bin/wget

# Copy binary from build stage
COPY --from=build /go/bin/app /app

# Expose necessary ports
EXPOSE 80

# Define entrypoint as the binary
ENTRYPOINT [ "/app" ]
