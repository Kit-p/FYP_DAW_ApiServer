// This file contains database queries used in command handler.

package command

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"

	"daw/apiserver/pkg/db"
)

var CommandResponseTableName string = "Command_Response"
var AngelboxTableName string = "Angelbox"

// dbInsertCommandResponseAndUpdateAngelboxVersion is a package private method to insert into database a command response and update the AngelBox version column if necessary.
func dbInsertCommandResponseAndUpdateAngelboxVersion(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, commandResponseMessage *CommandResponseMessage) error {
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"INSERT INTO %s.%s (command_id, angelbox_id, is_success, response, record_time) VALUES ($1, $2, $3, $4, $5)",
			db.DatabaseName,
			CommandResponseTableName,
		)

		response_bytes, err := json.Marshal(commandResponseMessage.Response)
		if err != nil {
			return err
		}

		_, err = tx.Exec(context.Background(),
			sql,
			commandResponseMessage.CommandId,
			commandResponseMessage.AngelboxId,
			commandResponseMessage.IsSuccess,
			string(response_bytes),
			commandResponseMessage.Timestamp,
		)

		sql = fmt.Sprintf(
			"UPDATE %s.%s SET version=$2 WHERE id=$1",
			db.DatabaseName,
			AngelboxTableName,
		)

		tx.Exec(context.Background(),
			sql,
			commandResponseMessage.AngelboxId,
			commandResponseMessage.Version,
		)

		return err
	}
	return crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
}
