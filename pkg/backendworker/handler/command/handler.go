// This file defines message handlers for command related topics.

package command

import (
	"daw/apiserver/pkg/backendworker/utils"
	"encoding/json"
	"errors"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.uber.org/zap"
	"golang.org/x/net/context"
)

// validateBasicCommandResponseMessage validates command response messages.
func validateBasicCommandResponseMessage(messageWrapperObject *CommandResponseMessageWrapper) bool {
	if messageWrapperObject == nil {
		return false
	}
	if messageWrapperObject.CommandResponseMessage == nil || messageWrapperObject.Raw_Timestamp == nil {
		return false
	}

	if messageWrapperObject.CommandId == nil ||
		messageWrapperObject.AngelboxId == nil ||
		messageWrapperObject.IsSuccess == nil {
		return false
	}

	return true
}

// parseCommandResponseMessage converts a raw message payload to CommandResponseMessage.
func parseCommandResponseMessage(msg mqtt.Message) (*CommandResponseMessage, error) {
	messageWrapperObject := &CommandResponseMessageWrapper{CommandResponseMessage: &CommandResponseMessage{}}

	if err := json.Unmarshal(msg.Payload(), messageWrapperObject); err != nil {
		return nil, err
	}

	if !validateBasicCommandResponseMessage(messageWrapperObject) {
		return nil, errors.New("invalid command response message format/value")
	}

	timestamp := time.UnixMilli(*messageWrapperObject.Raw_Timestamp)
	messageWrapperObject.Timestamp = &timestamp

	if messageWrapperObject.Raw_Response != nil {
		responseWrapperObject := &CommandResponseWrapper{CommandResponse: &CommandResponse{}}
		if err := json.Unmarshal([]byte(*messageWrapperObject.Raw_Response), responseWrapperObject); err != nil {
			return messageWrapperObject.CommandResponseMessage, err
		}
		if responseWrapperObject.Raw_Error != nil {
			responseWrapperObject.Error = utils.NewMarshalableError(errors.New(*responseWrapperObject.Raw_Error))
		}
		messageWrapperObject.Response = responseWrapperObject.CommandResponse
	}

	return messageWrapperObject.CommandResponseMessage, nil
}

// HandleCommandResponseMessage defines the message handler.
func HandleCommandResponseMessage(logger *zap.Logger, dbPool *pgxpool.Pool, msg mqtt.Message) {
	messageObject, err := parseCommandResponseMessage(msg)
	if messageObject == nil || err != nil {
		logger.Error("fail to parse mqtt message",
			zap.Error(err),
		)
		return
	}

	if err := dbInsertCommandResponseAndUpdateAngelboxVersion(context.Background(), dbPool, pgx.TxOptions{}, messageObject); err != nil {
		logger.Debug("fail to insert command response",
			zap.Error(err),
		)
		return
	}
	logger.Debug("successfully process and insert command response")
}
