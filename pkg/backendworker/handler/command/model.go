// This file contains types and corresponding JSON schemas used in the command handler.

package command

import (
	"daw/apiserver/pkg/backendworker/utils"
	"time"
)

// CommandMessageData holds custom data for a command message.
type CommandMessageData struct {
	UpdateUrl *string `json:"update_url,omitempty"`
}

// CommandMessage holds a command message.
type CommandMessage struct {
	Id      *string             `json:"id"`
	Command *int                `json:"command"`
	Data    *CommandMessageData `json:"data"`
}

// CommandMessageWrapper holds a command message with raw data.
type CommandMessageWrapper struct {
	*CommandMessage
	Raw_Data *string `json:"data,omitempty"`
}

// CommandResponseMessage holds a command response message.
type CommandResponseMessage struct {
	Version    *string          `json:"version"`
	CommandId  *string          `json:"command_id"`
	AngelboxId *string          `json:"angelbox_id"`
	IsSuccess  *bool            `json:"is_success"`
	Response   *CommandResponse `json:"response"`
	Timestamp  *time.Time       `json:"timestamp"`
}

// CommandResponseMessageWrapper holds a command response message with raw response message and timestamp.
type CommandResponseMessageWrapper struct {
	*CommandResponseMessage
	Raw_Response  *string `json:"response,omitempty"`
	Raw_Timestamp *int64  `json:"timestamp,omitempty"`
}

// CommandResponse holds a command response message content.
type CommandResponse struct {
	Message *string                 `json:"msg,omitempty"`
	Error   *utils.MarshalableError `json:"err,omitempty"`
}

// CommandResponseWrapper holds a command response message content with raw error message.
type CommandResponseWrapper struct {
	*CommandResponse
	Raw_Error *string `json:"err,omitempty"`
}
