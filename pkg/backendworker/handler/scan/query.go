// This file contains database queries used in scan result and heartbeat handler.

package scan

import (
	"context"
	"fmt"
	"time"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"

	"daw/apiserver/pkg/db"
)

var AngelboxTableName string = "AngelBox"
var AngelboxStatusTableName string = "AngelBox_Status"
var HeartbeatTableName string = "Heartbeat"
var IssueTableName string = "Issue"
var ScanResultTableName string = "Scan_Result"

var ServerSettingTableName string = "Error_Scan_Setting"
var MaxGpsNoUpdateTimeKey string = "max_gps_no_update_time"

// dbGetAngelbox is a package private method to query database an AngelBox with the specific angelboxId.
func dbGetAngelbox(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string) (*Angelbox, error) {
	var angelbox *Angelbox = &Angelbox{}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT id, mac_address, init_x, init_y, init_z, location_en, location_zh, gps, state, create_time FROM %s.%s WHERE id=$1",
			db.DatabaseName,
			AngelboxTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
			angelboxId,
		).Scan(&angelbox.ID, &angelbox.Mac_Address, &angelbox.InitX, &angelbox.InitY, &angelbox.InitZ, &angelbox.Location_EN, &angelbox.Location_ZH, &angelbox.GPS, &angelbox.State, &angelbox.CreateTime)

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return angelbox, err
}

// dbGetAngelboxStatus is a package private method to query database the real-time status of an AngelBox with the specific angelboxId.
func dbGetAngelboxStatus(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string) (*AngelboxStatus, error) {
	var angelboxStatus *AngelboxStatus = &AngelboxStatus{Angelbox: &Angelbox{}}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT id, mac_address, init_x, init_y, init_z, location_en, location_zh, gps, state, create_time, gps_time, last_active FROM %s.%s WHERE id=$1",
			db.DatabaseName,
			AngelboxStatusTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
			angelboxId,
		).Scan(&angelboxStatus.ID, &angelboxStatus.Mac_Address, &angelboxStatus.InitX, &angelboxStatus.InitY, &angelboxStatus.InitZ, &angelboxStatus.Location_EN, &angelboxStatus.Location_ZH, &angelboxStatus.GPS, &angelboxStatus.State, &angelboxStatus.CreateTime, &angelboxStatus.LastGpsTime, &angelboxStatus.LastActiveTime)

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return angelboxStatus, err
}

// dbInsertHeartbeat is a package private method to insert into database a new heartbeat using data in angelboxMessage.
func dbInsertHeartbeat(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxMessage *HeartbeatMessage) (*string, error) {
	id := ""

	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"INSERT INTO %s.%s (id, angelbox_id, current_x, current_y, current_z, gps_error, gps_time, error_codes, record_time) VALUES (default, $1, $2, $3, $4, $5, $6, $7, $8) returning id",
			db.DatabaseName,
			HeartbeatTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
			angelboxMessage.AngelboxId,
			angelboxMessage.LocationX,
			angelboxMessage.LocationY,
			angelboxMessage.LocationZ,
			angelboxMessage.Err2D,
			angelboxMessage.GpsTimestamp,
			angelboxMessage.ErrorCodes,
			angelboxMessage.Timestamp,
		).Scan(&id)

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	if id == "" {
		return nil, err
	}
	return &id, err
}

// dbInsertScanResults is a package private method to insert into database a new scan result using data in result.
func dbInsertScanResults(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, result *BeaconScanMessage) error {
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"INSERT INTO %s.%s (angelbox_id, mac_address, beacon_uuid, major, minor, rssi, location_x, location_y, location_z, gps_error, gps_time, record_time) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)",
			db.DatabaseName,
			ScanResultTableName,
		)

		_, err := tx.Exec(context.Background(),
			sql,
			result.AngelboxId,
			result.MacAddr,
			result.UUID,
			result.Major,
			result.Minor,
			result.RSSI,
			result.LocationX,
			result.LocationY,
			result.LocationZ,
			result.Err2D,
			result.GpsTimestamp,
			result.Timestamp,
		)
		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return err
}

// _dbResolveAngelBoxIssue is a private method to update database the resolution of an AngelBox issue.
func _dbResolveAngelBoxIssue(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string, errorCode ErrorCode) error {
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"UPDATE %s.%s set resolved=true, resolved_time=current_timestamp(), resolved_manually=false where type='%s'::issue_type and angelbox_id=$1 and error_code=%d and resolved=false",
			db.DatabaseName,
			IssueTableName,
			ANGELBOX,
			errorCode,
		)

		_, err := tx.Exec(context.Background(),
			sql,
			angelboxId,
		)
		return err
	}
	return crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
}

// _dbInsertAngelBoxIssue is a private method to insert into database a new AngelBox issue.
func _dbInsertAngelBoxIssue(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string, heartbeatId *string, errorCode ErrorCode, detail_en string, detail_zh string, createTime *time.Time) error {
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"with g as (select i.group_id from %s.%s i where i.type = $1::issue_type and i.angelbox_id = $2::uuid and i.error_code = $3 and i.resolved = false limit 1), v as (select gid.group_id as group_id, $1::issue_type as type, $2::uuid as angelbox_id, $3 as error_code, $4::uuid as heartbeat_id, '%s' collate en as detail_en, '%s' collate zh as detail_zh, $5::timestamptz as create_time from (select case when exists (select 1 from g) then (select group_id from g limit 1) else gen_random_ulid() end as group_id) gid) insert into %s.%s (group_id, type, angelbox_id, error_code, heartbeat_id, detail_en, detail_zh, create_time) select group_id, type, angelbox_id, error_code, heartbeat_id, detail_en, detail_zh, create_time from v",
			db.DatabaseName,
			IssueTableName,
			detail_en,
			detail_zh,
			db.DatabaseName,
			IssueTableName,
		)

		if createTime == nil {
			_createTime := time.Now()
			createTime = &_createTime
		}

		_, err := tx.Exec(context.Background(),
			sql,
			ANGELBOX,
			angelboxId,
			errorCode,
			heartbeatId,
			createTime,
		)
		return err
	}
	return crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
}

// dbResolveNotActiveIssue is wrapper of _dbResolveAngelBoxIssue to resolve not active issues of AngelBox with the specific angelboxId.
func dbResolveNotActiveIssue(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string) error {
	return _dbResolveAngelBoxIssue(ctx, conn, txOptions, angelboxId, NOT_ACTIVE)
}

// dbResolveBluetoothIssue is wrapper of _dbResolveAngelBoxIssue to resolve bluetooth failure issues of AngelBox with the specific angelboxId.
func dbResolveBluetoothIssue(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string) error {
	return _dbResolveAngelBoxIssue(ctx, conn, txOptions, angelboxId, BLUETOOTH_FAILURE)
}

// dbInsertBluetoothIssue is wrapper of _dbInsertAngelBoxIssue to insert bluetooth failure issues of AngelBox with the specific angelboxId.
func dbInsertBluetoothIssue(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string, heartbeatId *string, createTime *time.Time) error {
	detail_en := "The bluetooth hardware/thread of this AngelBox failed"
	detail_zh := "这个 AngelBox 的蓝牙硬件/线程失败"
	return _dbInsertAngelBoxIssue(ctx, conn, txOptions, angelboxId, heartbeatId, BLUETOOTH_FAILURE, detail_en, detail_zh, createTime)
}

// dbResolveGpsFailureIssue is wrapper of _dbResolveAngelBoxIssue to resolve GPS failure issues of AngelBox with the specific angelboxId.
func dbResolveGpsFailureIssue(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string) error {
	return _dbResolveAngelBoxIssue(ctx, conn, txOptions, angelboxId, GPS_FAILURE)
}

// dbInsertGpsFailureIssue is wrapper of _dbInsertAngelBoxIssue to insert GPS failure issues of AngelBox with the specific angelboxId.
func dbInsertGpsFailureIssue(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string, heartbeatId *string, createTime *time.Time) error {
	detail_en := "The location(GPS) hardware/thread of this AngelBox failed"
	detail_zh := "这个 AngelBox 的位置(GPS)硬件/线程失败"
	return _dbInsertAngelBoxIssue(ctx, conn, txOptions, angelboxId, heartbeatId, GPS_FAILURE, detail_en, detail_zh, createTime)
}

// dbGetMaxGpsNoUpdateTime is a package private method to query database the threshold time for GPS no update issue.
func dbGetMaxGpsNoUpdateTime(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions) (*IntServerSetting, error) {
	var setting *IntServerSetting = &IntServerSetting{}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT key, value FROM %s.%s WHERE key='%s' limit 1",
			db.DatabaseName,
			ServerSettingTableName,
			MaxGpsNoUpdateTimeKey,
		)

		err := tx.QueryRow(context.Background(),
			sql,
		).Scan(&setting.Key, &setting.Value)

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return setting, err
}

// dbResolveGpsNoUpdateIssue is wrapper of _dbResolveAngelBoxIssue to resolve GPS no update issues of AngelBox with the specific angelboxId.
func dbResolveGpsNoUpdateIssue(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string) error {
	return _dbResolveAngelBoxIssue(ctx, conn, txOptions, angelboxId, GPS_NO_UPDATE)
}

// dbInsertGpsNoUpdateIssue is wrapper of _dbInsertAngelBoxIssue to insert GPS no update issues of AngelBox with the specific angelboxId.
func dbInsertGpsNoUpdateIssue(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string, lastGpsUpdate *time.Time, startupTime *time.Time, heartbeatId *string, createTime *time.Time) error {
	var detail_en string
	var detail_zh string

	// create detail according to last active / create time (if 0 heartbeat)
	if lastGpsUpdate != nil {
		lastUpdateTimeString := lastGpsUpdate.Local().String()
		detail_en = fmt.Sprintf("This angelbox have no gps update for since %s", lastUpdateTimeString)
		detail_zh = fmt.Sprintf("这个angelbox从此 %s 没有位置更新", lastUpdateTimeString)
	} else {
		startupTimeString := startupTime.Local().String()
		detail_en = fmt.Sprintf("This angelbox have no gps update for since %s", startupTimeString)
		detail_zh = fmt.Sprintf("这个angelbox从此 %s 没有位置更新", startupTimeString)
	}

	return _dbInsertAngelBoxIssue(ctx, conn, txOptions, angelboxId, heartbeatId, GPS_NO_UPDATE, detail_en, detail_zh, createTime)
}
