// This file contains types and corresponding JSON schemas used in the scan result and heartbeat handler.

package scan

import (
	"encoding/base64"
	"strconv"
	"time"
)

type IssueType string

// IssueType constants
const (
	ANGELBOX IssueType = "angelbox" // AngelBox related
	SERVER   IssueType = "server"   // server related (unused)
	UNKNOWN  IssueType = "unknown"  // unknown (unused)
)

type ErrorCode int

// ErrorCode constants
const (
	NOT_ACTIVE        ErrorCode = 100
	BLUETOOTH_FAILURE ErrorCode = 101
	GPS_FAILURE       ErrorCode = 102
	GPS_NO_UPDATE     ErrorCode = 103
)

// IntServerSetting holds error scanner configuration items.
type IntServerSetting struct {
	Key   string `json:"key"`
	Value int64  `json:"value"`
}

// HeartbeatMessage holds a heartbeat message.
type HeartbeatMessage struct {
	Version          *string    `json:"version"`
	AngelboxId       *string    `json:"angelboxId"`
	LocationX        *float64   `json:"locationX"`
	LocationY        *float64   `json:"locationY"`
	LocationZ        *float64   `json:"locationZ"`
	Err2D            *float64   `json:"err2D"`
	GpsTimestamp     *time.Time `json:"gpsTimestamp"`
	Timestamp        *time.Time `json:"timestamp"`
	ErrorCodes       []int      `json:"errorCodes"`
	StartupTimestamp *time.Time `json:"startupTimestamp"`
}

// HeartbeatMessage holds a heartbeat message with raw timestamps.
type HearbeatMessageWrapper struct {
	*HeartbeatMessage
	Raw_GpsTimestamp     *int64 `json:"gpsTimestamp,omitempty"`
	Raw_Timestamp        *int64 `json:"timestamp,omitempty"`
	Raw_StartupTimestamp *int64 `json:"startupTimestamp,omitempty"`
}

// BeaconScanMessage holds a scan result message.
type BeaconScanMessage struct {
	// check when received msg
	Version    *string    `json:"version"`
	AngelboxId *string    `json:"angelboxId"`
	Timestamp  *time.Time `json:"timestamp"`
	MacAddr    *string    `json:"macAddr"`
	UUID       *string    `json:"uuid"`
	Major      *int       `json:"major"`
	Minor      *int       `json:"minor"`
	RSSI       *int       `json:"rssi"`
	TxPower    *int       `json:"txPower"`
	// filled or checked when parseScanResults
	LocationX        *float64   `json:"locationX"`
	LocationY        *float64   `json:"locationY"`
	LocationZ        *float64   `json:"locationZ"`
	Err2D            *float64   `json:"err2D"`
	GpsTimestamp     *time.Time `json:"gpsTimestamp"`
	ErrorCodes       []int      `json:"errorCodes"`
	StartupTimestamp *time.Time `json:"startupTimestamp"`
}

// BeaconScanMessageWrapper holds a scan result message with raw timestamps.
type BeaconScanMessageWrapper struct {
	*BeaconScanMessage
	Raw_GpsTimestamp     *int64 `json:"gpsTimestamp,omitempty"`
	Raw_Timestamp        *int64 `json:"timestamp,omitempty"`
	Raw_StartupTimestamp *int64 `json:"startupTimestamp,omitempty"`
}

// Angelbox holds a AngelBox details.
type Angelbox struct {
	ID          *string    `json:"id"`
	Mac_Address *string    `json:"mac_address"`
	InitX       *float64   `json:"init_x"`
	InitY       *float64   `json:"init_y"`
	InitZ       *float64   `json:"init_z"`
	Location_EN *string    `json:"location_en"`
	Location_ZH *string    `json:"location_zh"`
	GPS         *bool      `json:"gps"`
	State       *string    `json:"state"`
	CreateTime  *time.Time `json:"create_time"`
}

// AngelboxStatus holds a AngelBox details with real-time status.
type AngelboxStatus struct {
	*Angelbox
	LastGpsTime    *time.Time `json:"gps_time"`
	LastActiveTime *time.Time `json:"last_active"`
}

// ForwardDataEntry holds message object for forwarding to existing API from past FYP.
type ForwardDataEntry struct {
	Type      *int     `json:"type"`
	RMAC      *string  `json:"rmac"`
	TMAC      *string  `json:"tmac"`
	RSSI      *int     `json:"rssi"`
	UUID      *string  `json:"uuid"`
	Latitude  *string  `json:"latitude"`
	Longitude *string  `json:"longitude"`
	Gpsacc    *float64 `json:"gpsacc"`
	Ts        *string  `json:"ts"`
	Gpstime   *string  `json:"gpstime"`
}

// NewForwardDataEntry is a factory method to create ForwardDataEntry.
func NewForwardDataEntry(boxMacAddress string, entry BeaconScanMessage) *ForwardDataEntry {
	var identifier string = strconv.Itoa(*entry.Major) + strconv.Itoa(*entry.Minor)
	var beaconType int = 2
	var base64Latitude string = base64.StdEncoding.EncodeToString([]byte(strconv.FormatFloat(*entry.LocationY, 'f', -1, 64)))
	var base64Longitude string = base64.StdEncoding.EncodeToString([]byte(strconv.FormatFloat(*entry.LocationX, 'f', -1, 64)))
	var detectionTimestamp string = strconv.FormatInt(entry.Timestamp.Unix(), 10)
	var GpsTimestamp string = strconv.FormatInt(entry.GpsTimestamp.Unix(), 10)

	var forwardEntry ForwardDataEntry = ForwardDataEntry{
		Type:      &beaconType,
		RMAC:      &boxMacAddress,
		TMAC:      &identifier,
		RSSI:      entry.RSSI,
		UUID:      &identifier,
		Latitude:  &base64Latitude,
		Longitude: &base64Longitude,
		Gpsacc:    entry.Err2D,
		Ts:        &detectionTimestamp,
		Gpstime:   &GpsTimestamp}

	return &forwardEntry
}
