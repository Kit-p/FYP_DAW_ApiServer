// This file defines message handlers for scan result and heartbeat related topics.

package scan

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.uber.org/zap"
)

// validateEnabledAngelbox validates if AngelBox is valid and enabled.
func validateEnabledAngelbox(box *Angelbox) bool {
	if box == nil {
		return false
	}

	// check all basic fields exist
	if box.ID == nil ||
		box.Mac_Address == nil ||
		box.Location_EN == nil ||
		box.Location_ZH == nil ||
		box.GPS == nil ||
		box.State == nil ||
		box.CreateTime == nil {
		return false
	}

	// static box but no coordinates
	if !*box.GPS && (box.InitX == nil || box.InitY == nil) {
		return false
	}

	if *box.State != "enabled" {
		return false
	}

	return true
}

// validateStaticAngelbox validates if AngelBox is valid and static.
func validateStaticAngelbox(box *Angelbox) bool {
	if box == nil {
		return false
	}

	// check all fields exist
	if box.ID == nil ||
		box.Mac_Address == nil ||
		box.InitX == nil ||
		box.InitY == nil ||
		box.Location_EN == nil ||
		box.Location_ZH == nil ||
		box.GPS == nil ||
		box.State == nil ||
		box.CreateTime == nil {
		return false
	}
	return true
}

// validateBasicHeartbeatMessage validates a heartbeat message.
func validateBasicHeartbeatMessage(messageWrapperObject *HearbeatMessageWrapper) bool {
	if messageWrapperObject == nil {
		return false
	}

	// check heartbeat message has timestamp
	if messageWrapperObject.HeartbeatMessage == nil ||
		messageWrapperObject.Raw_Timestamp == nil ||
		messageWrapperObject.Raw_StartupTimestamp == nil {
		return false
	}

	// check heartbeat message has AngelBox ID.
	if messageWrapperObject.AngelboxId == nil {
		return false
	}

	return true
}

// validateBasicScanMessage validates a scan result message.
func validateBasicScanMessage(messageWrapperObject *BeaconScanMessageWrapper) bool {
	if messageWrapperObject == nil {
		return false
	}
	if messageWrapperObject.BeaconScanMessage == nil ||
		messageWrapperObject.Raw_Timestamp == nil ||
		messageWrapperObject.Raw_StartupTimestamp == nil {
		return false
	}

	// check required field
	if messageWrapperObject.AngelboxId == nil ||
		messageWrapperObject.MacAddr == nil ||
		messageWrapperObject.UUID == nil ||
		messageWrapperObject.Major == nil ||
		messageWrapperObject.Minor == nil ||
		messageWrapperObject.RSSI == nil ||
		messageWrapperObject.TxPower == nil {
		return false
	}

	return true
}

// validateBeaconScanGpsInfo validates the GPS information of a scan result message.
func validateBeaconScanGpsInfo(entry BeaconScanMessage) bool {
	if entry.LocationX == nil ||
		entry.LocationY == nil ||
		entry.Err2D == nil ||
		entry.GpsTimestamp == nil {
		return false
	}

	if *entry.LocationX > 180 ||
		*entry.LocationX < -180 ||
		*entry.LocationY > 90 ||
		*entry.LocationY < -90 {
		return false
	}

	return true
}

// ErrorCodeContains checks if an array of errorCodes include a specific error code e.
func ErrorCodeContains(errorCodes []int, e int) bool {
	for _, v := range errorCodes {
		if v == e {
			return true
		}
	}

	return false
}

// removeErrorCode removes a specific error code e from an array of errorCodes.
func removeErrorCode(errorCodes []int, e int) []int {
	for i, v := range errorCodes {
		if v == e {
			return append(errorCodes[:i], errorCodes[i+1:]...)
		}
	}
	return errorCodes
}

// parseScanMessage converts raw MQTT message to BeaconScanMessage.
func parseScanMessage(msg mqtt.Message) (*BeaconScanMessage, error) {
	var messageWrapperObject *BeaconScanMessageWrapper = &BeaconScanMessageWrapper{BeaconScanMessage: &BeaconScanMessage{}}

	err := json.Unmarshal(msg.Payload(), messageWrapperObject)
	if err != nil {
		return nil, err
	}

	if !validateBasicScanMessage(messageWrapperObject) {
		return nil, errors.New("invalid scan message format/value")
	}

	timestamp := time.UnixMilli(*messageWrapperObject.Raw_Timestamp)
	messageWrapperObject.Timestamp = &timestamp

	startupTimestamp := time.UnixMilli(*messageWrapperObject.Raw_StartupTimestamp)
	messageWrapperObject.StartupTimestamp = &startupTimestamp

	if messageWrapperObject.Raw_GpsTimestamp != nil {
		gpsTimestamp := time.UnixMilli(*messageWrapperObject.Raw_GpsTimestamp)
		messageWrapperObject.GpsTimestamp = &gpsTimestamp
	}

	return messageWrapperObject.BeaconScanMessage, nil
}

// forwardScanResults forwards the scan result to the API in the existing API of the mobile app server from the previous FYP.
func forwardScanResults(boxMacAddress string, result *BeaconScanMessage, mobile_app_server_api_endpoint string) error {
	var forwardDataList = []ForwardDataEntry{}

	forwardDataList = append(forwardDataList, *NewForwardDataEntry(boxMacAddress, *result))

	buf := new(bytes.Buffer)
	json.NewEncoder(buf).Encode(forwardDataList)

	_, err := http.Post(mobile_app_server_api_endpoint, "application/merge-patch+json", buf)

	if err != nil {
		return err
	}

	return nil
}

// HandleScanMessage defines the message handler.
func HandleScanMessage(logger *zap.Logger, dbPool *pgxpool.Pool, msg mqtt.Message, upstreamEndpoint string) {
	startProcessTime := time.Now().Local()
	// logger.Debug("Start scan result", zap.Time("Start", startProcessTime))

	// parse scan message
	messageObject, err := parseScanMessage(msg)
	if messageObject == nil || err != nil {
		logger.Error("fail to parse mqtt message",
			zap.Error(err),
		)
		return
	}

	// get box info from db
	box, err := dbGetAngelbox(context.Background(), dbPool, pgx.TxOptions{}, *messageObject.AngelboxId)
	if err != nil {
		logger.Error("fail to get angelbox info from db",
			zap.Error(err),
		)
		return
	}

	// filter to process only valid enabled angelbox
	if box == nil || !validateEnabledAngelbox(box) {
		return
	}

	// fill info for static box & remove gps failure error code
	if !*box.GPS {
		// static box
		if !validateStaticAngelbox(box) {
			logger.Error("static angelbox info is not valid", zap.String("angelbox_id", *box.ID))
			return
		}
		// fill angelbox location to scan message (not include scan result)
		messageObject.LocationX = box.InitX
		messageObject.LocationY = box.InitY
		messageObject.LocationZ = box.InitZ
		staticBoxErr2D := 30.0
		messageObject.Err2D = &staticBoxErr2D
		messageObject.GpsTimestamp = messageObject.Timestamp

		messageObject.ErrorCodes = removeErrorCode(messageObject.ErrorCodes, int(GPS_FAILURE))
	}

	if validateBeaconScanGpsInfo(*messageObject) {
		err = forwardScanResults(*box.Mac_Address, messageObject, upstreamEndpoint)
		if err != nil {
			logger.Error("fail to forward scan result",
				zap.Error(err),
				zap.String("angelbox_id", *box.ID),
			)
		}
	}

	err = dbInsertScanResults(context.Background(), dbPool, pgx.TxOptions{}, messageObject)
	if err != nil {
		logger.Debug("fail to insert scan results",
			zap.Error(err),
			zap.String("angelbox_id", *box.ID),
		)
		return
	}

	endProcessTime := time.Now().Local()
	logger.Debug("successfully process, insert, and forward scan result", zap.Time("End", startProcessTime), zap.Time("End", endProcessTime), zap.String("Duration", endProcessTime.Sub(startProcessTime).String()))
}

// parseHeartbeatMessage converts raw MQTT message to HeartbeatMessage.
func parseHeartbeatMessage(msg mqtt.Message) (*HeartbeatMessage, error) {
	var messageWrapperObject *HearbeatMessageWrapper = &HearbeatMessageWrapper{HeartbeatMessage: &HeartbeatMessage{}}

	err := json.Unmarshal(msg.Payload(), messageWrapperObject)
	if err != nil {
		return nil, err
	}

	if !validateBasicHeartbeatMessage(messageWrapperObject) {
		return nil, errors.New("invalid message format/value")
	}

	//parse raw timestamps
	timestamp := time.UnixMilli(*messageWrapperObject.Raw_Timestamp)
	messageWrapperObject.Timestamp = &timestamp

	startupTimestamp := time.UnixMilli(*messageWrapperObject.Raw_StartupTimestamp)
	messageWrapperObject.StartupTimestamp = &startupTimestamp

	if messageWrapperObject.Raw_GpsTimestamp != nil {
		gpsTimestamp := time.UnixMilli(*messageWrapperObject.Raw_GpsTimestamp)
		messageWrapperObject.GpsTimestamp = &gpsTimestamp
	}

	return messageWrapperObject.HeartbeatMessage, nil
}

// HandleHeartbeatMessage defines the message handler.
func HandleHeartbeatMessage(logger *zap.Logger, dbPool *pgxpool.Pool, msg mqtt.Message, upstreamEndpoint string) {
	startProcessTime := time.Now().Local()
	// logger.Debug("Start processing heartbeat", zap.Time("Start", startProcessTime))

	// parse heartbeat message
	messageObject, err := parseHeartbeatMessage(msg)
	if messageObject == nil || err != nil {
		logger.Error("fail to parse mqtt message",
			zap.Error(err),
		)
		return
	}

	// debug print heartbeat object
	// jsonString, jsonErr := json.Marshal(messageObject)
	// if jsonErr == nil {
	// 	fmt.Println(string(jsonString))
	// }

	// get boxStatus info from db
	boxStatus, err := dbGetAngelboxStatus(context.Background(), dbPool, pgx.TxOptions{}, *messageObject.AngelboxId)
	if err != nil {
		logger.Error("fail to get angelbox info from db",
			zap.Error(err),
		)
		return
	}

	// filter to process only valid enabled angelbox
	if boxStatus == nil || !validateEnabledAngelbox(boxStatus.Angelbox) {
		return
	}

	// fill info for static box & remove gps failure error code
	if !*boxStatus.GPS {
		// static box
		if !validateStaticAngelbox(boxStatus.Angelbox) {
			logger.Error("static angelbox info is not valid",
				zap.String("angelbox_id", *boxStatus.ID),
			)
			return
		}
		// fill angelbox location to scan message (not include scan result)
		messageObject.LocationX = boxStatus.InitX
		messageObject.LocationY = boxStatus.InitY
		messageObject.LocationZ = boxStatus.InitZ
		staticBoxErr2D := 30.0
		messageObject.Err2D = &staticBoxErr2D
		messageObject.GpsTimestamp = messageObject.Timestamp

		messageObject.ErrorCodes = removeErrorCode(messageObject.ErrorCodes, int(GPS_FAILURE))
	}

	heartbeatId, err := dbInsertHeartbeat(context.Background(), dbPool, pgx.TxOptions{}, messageObject)
	if err != nil {
		logger.Debug("fail to insert heartbeat",
			zap.Error(err),
		)
		return
	}

	// have msg, resolve not active issue of this box
	ResolveNotActiveIssue(logger, dbPool, *boxStatus.ID)

	// insert or resolve bluetooth statup failure issue
	HandleBluetoothFailureIssue(logger, dbPool, *boxStatus.ID, messageObject.ErrorCodes, heartbeatId, messageObject.Timestamp)

	// insert or resolve gps hardware/thread failure
	HandleGpsFailureIssue(logger, dbPool, *boxStatus.ID, *boxStatus.GPS, messageObject.ErrorCodes, heartbeatId, messageObject.Timestamp)

	// insert or resolve gps no update issue
	HandleGpsNoUpdateIssue(logger, dbPool, *boxStatus.ID, messageObject.GpsTimestamp, messageObject.StartupTimestamp, heartbeatId, messageObject.Timestamp)

	endProcessTime := time.Now().Local()
	logger.Debug("successfully insert heartbeat", zap.Time("End", startProcessTime), zap.Time("End", endProcessTime), zap.String("Duration", endProcessTime.Sub(startProcessTime).String()))
}

// ResolveNotActiveIssue resolves any existing AngelBox not active issue in the database.
func ResolveNotActiveIssue(logger *zap.Logger, dbPool *pgxpool.Pool, angelboxId string) {
	err := dbResolveNotActiveIssue(context.Background(), dbPool, pgx.TxOptions{}, angelboxId)
	if err != nil {
		logger.Error("fail resolve not active issue",
			zap.Error(err),
			zap.String("angelbox_id", angelboxId),
		)
	}
}

// HandleBluetoothFailureIssue creates a new / resolves any existing AngelBox bluetooth failure issue in the database.
func HandleBluetoothFailureIssue(logger *zap.Logger, dbPool *pgxpool.Pool, angelboxId string, errorCodes []int, heartbeatId *string, createTime *time.Time) {
	// handle bluetooth failure error code exist/not exist
	if !ErrorCodeContains(errorCodes, int(BLUETOOTH_FAILURE)) {
		err := dbResolveBluetoothIssue(context.Background(), dbPool, pgx.TxOptions{}, angelboxId)
		if err != nil {
			logger.Error("fail resolve bluetooth failure issue",
				zap.Error(err),
				zap.String("angelbox_id", angelboxId),
			)
		}
	} else {
		err := dbInsertBluetoothIssue(context.Background(), dbPool, pgx.TxOptions{}, angelboxId, heartbeatId, createTime)
		if err != nil {
			logger.Debug("fail insert bluetooth failure issue",
				zap.Error(err),
				zap.String("angelbox_id", angelboxId),
			)
		}
	}
}

// HandleGpsFailureIssue creates a new / resolves any existing AngelBox GPS failure issue in the database.
func HandleGpsFailureIssue(logger *zap.Logger, dbPool *pgxpool.Pool, angelboxId string, gps bool, errorCodes []int, heartbeatId *string, createTime *time.Time) {
	if !ErrorCodeContains(errorCodes, int(GPS_FAILURE)) ||
		!gps {
		// resolve gps failure issue
		err := dbResolveGpsFailureIssue(context.Background(), dbPool, pgx.TxOptions{}, angelboxId)
		if err != nil {
			logger.Error("fail resolve gps failure issue",
				zap.Error(err),
				zap.String("angelbox_id", angelboxId),
			)
		}
	} else {
		// insert gps failure issue when there is error code and it is a mobile box
		err := dbInsertGpsFailureIssue(context.Background(), dbPool, pgx.TxOptions{}, angelboxId, heartbeatId, createTime)
		if err != nil {
			logger.Debug("fail insert gps failure issue",
				zap.Error(err),
				zap.String("angelbox_id", angelboxId),
			)
		}
	}

}

// HandleGpsNoUpdateIssue creates a new / resolves any existing AngelBox GPS no update issue in the database.
func HandleGpsNoUpdateIssue(logger *zap.Logger, dbPool *pgxpool.Pool, angelboxId string, lastGpsUpdate *time.Time, startupTime *time.Time, heartbeatId *string, createTime *time.Time) {
	gpsMaxNoUpdateTimeSetting, gpsMaxNoUpdateTimeSettingErr := dbGetMaxGpsNoUpdateTime(context.Background(), dbPool, pgx.TxOptions{})

	// successfully get setting
	if gpsMaxNoUpdateTimeSettingErr == nil && gpsMaxNoUpdateTimeSetting != nil && gpsMaxNoUpdateTimeSetting.Value > 0 {
		// last gps update threshold
		threshholdTime := createTime.Add(-time.Duration(gpsMaxNoUpdateTimeSetting.Value) * time.Minute)

		// if it have a gps timestamp and it is within threshold (i.e. new gps update)
		if lastGpsUpdate != nil && lastGpsUpdate.After(threshholdTime) {
			err := dbResolveGpsNoUpdateIssue(context.Background(), dbPool, pgx.TxOptions{}, angelboxId)
			if err != nil {
				logger.Error("fail resolve gps no update issue",
					zap.Error(err),
					zap.String("angelbox_id", angelboxId),
				)
			}
		} else if startupTime.Before(threshholdTime) {
			// ignore: startup time after threshold -> just start up angelbox
			// startup time before threshold && (no gps timestamp || gps time too old)
			// i.e. started for a period of time but still no gps / no gps update
			err := dbInsertGpsNoUpdateIssue(context.Background(), dbPool, pgx.TxOptions{}, angelboxId, lastGpsUpdate, startupTime, heartbeatId, createTime)
			if err != nil {
				logger.Debug("fail insert gps no update issue",
					zap.Error(err),
					zap.String("angelbox_id", angelboxId),
				)
			}
		}

	} else {
		// fail to get setting
		logger.Error(fmt.Sprintf("fail getting <%s> from database, skipping gps no update check", MaxGpsNoUpdateTimeKey),
			zap.Error(gpsMaxNoUpdateTimeSettingErr),
		)
	}
}
