// This files defines utility types for JSON conversion and common problems.

package utils

import (
	"encoding/json"
	"fmt"
)

type MarshalableError struct {
	err error
}

func (err *MarshalableError) Error() string {
	if err == nil || err.err == nil {
		return ""
	}
	return err.err.Error()
}

func (err MarshalableError) MarshalJSON() ([]byte, error) {
	return json.Marshal(err.Error())
}

func NewMarshalableError(err error) *MarshalableError {
	return &MarshalableError{err}
}

func MqttQueued(topic string) string {
	return fmt.Sprintf("$queue/%s", topic)
}
