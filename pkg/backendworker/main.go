// This file defines the constructor for the Backend Worker.

package backendworker

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.uber.org/zap"

	"daw/apiserver/pkg/backendworker/handler/command"
	"daw/apiserver/pkg/backendworker/handler/scan"
	"daw/apiserver/pkg/backendworker/utils"
)

// MQTT topic constants, for more detailed descriptions please reference the "DAW_DevOps" repository.
var (
	NAMESPACE     = "DAW"
	TEST_TOPIC    = fmt.Sprintf("%s/test", NAMESPACE)    // for testing only, no access control
	SCAN_TOPIC    = fmt.Sprintf("%s/scan", NAMESPACE)    // only angelbox can publish scan results, backendworker or admin can subscribe
	STATUS_TOPIC  = fmt.Sprintf("%s/status", NAMESPACE)  // only angelbox can publish heartbeats, backendworker or admin can subscribe
	COMMAND_TOPIC = fmt.Sprintf("%s/command", NAMESPACE) // only angelbox can publish responses, backendworker or admin can subscribe
	// COMMAND_TOPIC/{angelboxId} -> backendworker or admin can publish commands, angelbox or admin can subscribe
)

// BackendWorker object, includes a MQTT client, a logger, a database connection pool and other configuration items.
type BackendWorker struct {
	MqttClient       mqtt.Client
	UpstreamEndpoint string
	logger           *zap.Logger
	dbPool           *pgxpool.Pool
}

// Start starts the MQTT client, connects it to the broker and subscribes to topics.
func (worker *BackendWorker) Start() error {
	client, logger, dbPool := worker.MqttClient, worker.logger, worker.dbPool

	// connect to MQTT server
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		return token.Error()
	}
	logger.Info("connected to mqtt broker")

	// subscribe to heartbeat (status) topic
	if token := client.Subscribe(utils.MqttQueued(STATUS_TOPIC), 1, GetHeartbeatMessageHandler(worker.dbPool, worker.UpstreamEndpoint, worker.logger)); token.Wait() && token.Error() != nil {
		return token.Error()
	}
	logger.Info("subscribed to topic with queue", zap.String("topic", STATUS_TOPIC))

	// subscribe to scan topic
	if token := client.Subscribe(utils.MqttQueued(SCAN_TOPIC), 1, GetScanMessageHandler(dbPool, worker.UpstreamEndpoint, logger)); token.Wait() && token.Error() != nil {
		return token.Error()
	}
	logger.Info("subscribed to topic with queue", zap.String("topic", SCAN_TOPIC))

	// subscribe to command (response) topic
	if token := client.Subscribe(utils.MqttQueued(COMMAND_TOPIC), 1, GetCommandMessageHandler(dbPool, logger)); token.Wait() && token.Error() != nil {
		return token.Error()
	}
	logger.Info("subscribed to topic with queue", zap.String("topic", COMMAND_TOPIC))

	return nil
}

func (worker *BackendWorker) Stop() error {
	client, logger := worker.MqttClient, worker.logger

	// unsubscribe from heartbeat (status) topic
	if token := client.Unsubscribe(STATUS_TOPIC); token.Wait() && token.Error() != nil {
		return token.Error()
	}
	logger.Info("unsubscribed from topic", zap.String("topic", STATUS_TOPIC))

	// unsubscribe from scan topic
	if token := client.Unsubscribe(SCAN_TOPIC); token.Wait() && token.Error() != nil {
		return token.Error()
	}
	logger.Info("unsubscribed from topic", zap.String("topic", SCAN_TOPIC))

	// unsubscribe from command (response) topic
	if token := client.Unsubscribe(COMMAND_TOPIC); token.Wait() && token.Error() != nil {
		return token.Error()
	}
	logger.Info("unsubscribed from topic", zap.String("topic", COMMAND_TOPIC))

	// disconnect from MQTT server
	client.Disconnect(250)

	return nil
}

// PublishCommand sends a command to the AngelBoxes with ID in angelboxIds.
func (worker *BackendWorker) PublishCommand(id string, commandCode int, angelboxIds []string, data *string) (err error) {
	client, logger := worker.MqttClient, worker.logger
	if angelboxIds == nil || len(angelboxIds) <= 0 {
		return errors.New("cannot send command to empty angelboxIds")
	}

	// record the start time of the function
	timer := time.Now()

	// publish the command to each AngelBox ID in angelboxIds
	for _, angelboxId := range angelboxIds {
		// send to the topic COMMAND_TOPIC/{angelboxId}
		topic := fmt.Sprintf("%s/%s", COMMAND_TOPIC, angelboxId)

		// construct the message object
		commandMessage := &command.CommandMessageWrapper{CommandMessage: &command.CommandMessage{Id: &id, Command: &commandCode, Data: &command.CommandMessageData{}}, Raw_Data: data}
		if commandMessage.Raw_Data != nil {
			if err := json.Unmarshal([]byte(*commandMessage.Raw_Data), commandMessage.Data); err != nil {
				logger.Error("failed parsing command data",
					zap.Error(err),
				)
				return err
			}
		}
		message := commandMessage.CommandMessage

		// build to message payload by converting to JSON string
		payload, _ := json.Marshal(message)

		// publish the message
		client.Publish(topic, byte(2), false, payload)
	}

	// record the duration of the execution
	duration := time.Since(timer)
	logger.Debug("published command",
		zap.String("id", id),
		zap.Error(err),
		zap.Duration("took", duration),
		zap.Int("command", commandCode),
		zap.Strings("angelbox_ids", angelboxIds),
	)

	return nil
}

// GetScanMessageHandler returns a handler for handling scan result messages from AngelBoxes.
func GetScanMessageHandler(dbPool *pgxpool.Pool, mobile_app_server_api_endpoint string, logger *zap.Logger) mqtt.MessageHandler {
	return func(client mqtt.Client, msg mqtt.Message) {
		scan.HandleScanMessage(logger, dbPool, msg, mobile_app_server_api_endpoint)
	}
}

// GetHeartbeatMessageHandler returns a handler for handling heartbeat messages from AngelBoxes.
func GetHeartbeatMessageHandler(dbPool *pgxpool.Pool, mobile_app_server_api_endpoint string, logger *zap.Logger) mqtt.MessageHandler {
	return func(client mqtt.Client, msg mqtt.Message) {
		scan.HandleHeartbeatMessage(logger, dbPool, msg, mobile_app_server_api_endpoint)
	}
}

// GetCommandMessageHandler returns a handler for handling command response messages from AngelBoxes.
func GetCommandMessageHandler(dbPool *pgxpool.Pool, logger *zap.Logger) mqtt.MessageHandler {
	return func(client mqtt.Client, msg mqtt.Message) {
		command.HandleCommandResponseMessage(logger, dbPool, msg)
	}
}

// GetMessageHandler returns a default handler for all messages.
func GetMessageHandler(logger *zap.Logger) mqtt.MessageHandler {
	return func(client mqtt.Client, msg mqtt.Message) {
		logger.Debug("message received", zap.String("topic", msg.Topic()))
	}
}

// GetConnectHandler returns a default handler for the on connection event.
func GetConnectHandler(logger *zap.Logger) mqtt.OnConnectHandler {
	return func(client mqtt.Client) {
		logger.Info("mqtt connected/reconnected")
	}
}

// GetConnectLostHandler returns a default handler for the on connection lost event.
func GetConnectLostHandler(logger *zap.Logger) mqtt.ConnectionLostHandler {
	return func(client mqtt.Client, err error) {
		logger.Warn("mqtt connection losted")
	}
}

// NewTLSConfig returns the TLS configuration as required to connect to the MQTT broker.
func NewTLSConfig(logger *zap.Logger) *tls.Config {
	// import trusted certificates from CAfile.pem
	// alternatively, manually add CA certificates to default openssl CA bundle
	certpool := x509.NewCertPool()
	pemCerts, err := ioutil.ReadFile("certs/mqtt/ca.crt")
	if err == nil {
		certpool.AppendCertsFromPEM(pemCerts)
	} else {
		logger.Fatal("Fail loading CA file", zap.Error(err))
	}

	// import client certificate/key pair
	cert, err := tls.LoadX509KeyPair("certs/mqtt/client.backendworker.crt", "certs/mqtt/client.backendworker.key")
	if err != nil {
		logger.Fatal("Fail loading client certificate", zap.Error(err))
	}

	// just to print out the client certificate
	cert.Leaf, err = x509.ParseCertificate(cert.Certificate[0])
	if err != nil {
		logger.Fatal("Fail parsing client certificate", zap.Error(err))
	}

	// create tls.Config with desired tls properties
	return &tls.Config{
		RootCAs:            certpool,                // certs used to verify server cert
		ClientAuth:         tls.NoClientCert,        // whether to request cert from server
		ClientCAs:          nil,                     // certs used to validate client cert
		InsecureSkipVerify: true,                    // verify that cert contents match server, IP matches what is in cert etc
		Certificates:       []tls.Certificate{cert}, // list of certs client sends to server
	}
}

// NewBackendWorker is a factory method for creating a BackendWorker instance.
func NewBackendWorker(logger *zap.Logger, dbPool *pgxpool.Pool, upstreamEndpoint string, brokerAddr string, tlsConfig *tls.Config) (backendworker *BackendWorker, err error) {
	backendworker, err = nil, nil

	opts := mqtt.NewClientOptions()
	brokerProtocolTemplate := "tcp://%s"
	if tlsConfig != nil {
		opts.SetTLSConfig(tlsConfig)
		brokerProtocolTemplate = "ssl://%s"
	}
	brokerAddr = fmt.Sprintf(brokerProtocolTemplate, brokerAddr)
	opts.AddBroker(brokerAddr)
	opts.SetUsername("backendworker")
	opts.SetPassword("backendworker") // dummy password, authentication by mTLS
	opts.SetOrderMatters(false)       // for performance, lose in-order delivery
	opts.SetDefaultPublishHandler(GetMessageHandler(logger))
	opts.SetAutoReconnect(true) // to prevent disconnect due to unknown errors
	opts.SetOnConnectHandler(GetConnectHandler(logger))
	opts.SetConnectionLostHandler(GetConnectLostHandler(logger))
	client := mqtt.NewClient(opts)

	backendworker = &BackendWorker{MqttClient: client, UpstreamEndpoint: upstreamEndpoint, logger: logger, dbPool: dbPool}
	return backendworker, backendworker.Start()
}
