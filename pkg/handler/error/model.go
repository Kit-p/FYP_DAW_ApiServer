// This file contains types and corresponding JSON schemas used as error responses (4XX - 5XX) in all APIs.

package error

import (
	"net/http"

	"github.com/go-chi/render"
)

// ErrRes example
type ErrRes struct {
	Err        error `json:"-"`
	StatusCode int   `json:"-"`

	StatusText string `json:"status"`
	Code       int64  `json:"code,omitempty"`
	Error      string `json:"error,omitempty"`
}

// Render defines the post-processing of ErrRes before converting to JSON response body.
func (e *ErrRes) Render(res http.ResponseWriter, req *http.Request) error {
	// change the HTTP response status code
	render.Status(req, e.StatusCode)
	return nil
}

// ErrInvalidRequest is the factory method for ErrRes with status code 400 (Bad Request).
func ErrInvalidRequest(err error) render.Renderer {
	return &ErrRes{
		Err:        err,
		StatusCode: http.StatusBadRequest,
		StatusText: "Invalid request.",
		Error:      err.Error(),
	}
}

// ErrUnauthorized is the factory method for ErrRes with status code 401 (Unauthorized).
func ErrUnauthorized(err error) render.Renderer {
	return &ErrRes{
		Err:        err,
		StatusCode: http.StatusUnauthorized,
		StatusText: "Unauthorized.",
		Error:      err.Error(),
	}
}

// ErrForbidden is the factory method for ErrRes with status code 403 (Forbidden).
func ErrForbidden(err error) render.Renderer {
	return &ErrRes{
		Err:        err,
		StatusCode: http.StatusForbidden,
		StatusText: "Forbidden.",
		Error:      err.Error(),
	}
}

// ErrInternal is the factory method for ErrRes with status code 500 (Internal Server Error).
func ErrInternal(err error) render.Renderer {
	return &ErrRes{
		Err:        err,
		StatusCode: http.StatusInternalServerError,
		StatusText: "Internal server error.",
		Error:      err.Error(),
	}
}

// ErrRender is the factory method for ErrRes with status code 422 (Unprocessable Entity).
func ErrRender(err error) render.Renderer {
	return &ErrRes{
		Err:        err,
		StatusCode: http.StatusUnprocessableEntity,
		StatusText: "Error rendering response.",
		Error:      err.Error(),
	}
}

// ErrNotFound is the customized constant of ErrRes with status code 404 (Not Found).
var ErrNotFound = &ErrRes{StatusCode: http.StatusNotFound, StatusText: "Not found."}
