// This file contains database queries used in handlers for the /commands/* APIs.

package command

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"

	"daw/apiserver/pkg/db"
)

var CommandTableName string = "Command_Log"
var CommandResponseTableName string = "Command_Response"

// dbGetCommands is a package private method to query database for all command logs.
func dbGetCommands(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions) ([]*Command, error) {
	var commands []*Command
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT id, command, angelbox_ids, data, user_id, create_time FROM %s.%s ORDER BY create_time DESC",
			db.DatabaseName,
			CommandTableName,
		)

		rows, err := tx.Query(context.Background(),
			sql,
		)

		for rows.Next() {
			var command *CommandWrapper = &CommandWrapper{Command: &Command{}}
			rows.Scan(&command.ID, &command.CommandCode, &command.AngelboxIds, &command.Raw_Data, &command.UserId, &command.CreateTime)
			if command.Raw_Data != nil {
				var data *CommandData = &CommandData{}
				if err := json.Unmarshal([]byte(*command.Raw_Data), data); err != nil {
					return err
				} else {
					command.Data = data
				}
			}
			commands = append(commands, command.Command)
		}

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return commands, err
}

// dbGetCommand is a package private method to query database for a command log with the specific commandId.
func dbGetCommand(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, commandId string) (*Command, error) {
	var command *CommandWrapper = &CommandWrapper{Command: &Command{}}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT id, command, angelbox_ids, data, user_id, create_time FROM %s.%s WHERE id=$1",
			db.DatabaseName,
			CommandTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
			commandId,
		).Scan(&command.ID, &command.CommandCode, &command.AngelboxIds, &command.Raw_Data, &command.UserId, &command.CreateTime)
		if command.Raw_Data != nil {
			var data *CommandData = &CommandData{}
			if err := json.Unmarshal([]byte(*command.Raw_Data), data); err != nil {
				return err
			} else {
				command.Data = data
			}
		}

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return command.Command, err
}

// dbGetCommandResponses is a package private method to query database for all the responses of a command log with the specific commandId.
func dbGetCommandResponses(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, commandId string) ([]*CommandResponse, error) {
	var commandResponses []*CommandResponse
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT angelbox_id, is_success, response, record_time FROM %s.%s WHERE command_id=$1 ORDER BY record_time DESC",
			db.DatabaseName,
			CommandResponseTableName,
		)

		rows, err := tx.Query(context.Background(),
			sql,
			commandId,
		)

		for rows.Next() {
			var commandResponse *CommandResponseWrapper = &CommandResponseWrapper{CommandResponse: &CommandResponse{}}
			rows.Scan(&commandResponse.AngelboxId, &commandResponse.IsSuccess, &commandResponse.Raw_Response, &commandResponse.RecordTime)
			var response *CommandResponseBody = &CommandResponseBody{}
			if commandResponse.Raw_Response == nil {
				rawResponse := "{\"err\": \"an unknown error has occurred\"}"
				commandResponse.Raw_Response = &rawResponse
			}
			if err := json.Unmarshal([]byte(*commandResponse.Raw_Response), response); err != nil {
				return err
			}
			commandResponse.Response = response
			commandResponses = append(commandResponses, commandResponse.CommandResponse)
		}

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return commandResponses, err
}

// dbCreateCommand is a package private method to insert into database a new command log using data in command.
func dbCreateCommand(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, command *Command) (id string, err error) {
	id, err = "", nil
	if command == nil {
		return "", errors.New("parameter is nil")
	}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"INSERT INTO %s.%s (command, angelbox_ids, data, user_id, create_time) VALUES ($1, $2, $3, $4, $5) RETURNING id",
			db.DatabaseName,
			CommandTableName,
		)

		var data interface{} = command.Data
		if command.Data != nil {
			if bytes, err := json.Marshal(command.Data); err != nil {
				return err
			} else {
				data = bytes
			}
		}

		err := tx.QueryRow(context.Background(),
			sql,
			command.CommandCode,
			command.AngelboxIds,
			data,
			command.UserId,
			command.CreateTime,
		).Scan(&id)
		return err
	}
	err = crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return id, err
}
