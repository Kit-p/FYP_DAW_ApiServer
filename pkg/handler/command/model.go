// This file contains types and corresponding JSON schemas used in /commands/* APIs.

package command

import (
	"daw/apiserver/pkg/handler/util"
	"errors"
	"net/http"
	"time"

	"github.com/go-chi/render"
)

// CommandData example
type CommandData struct {
	UpdateUrl *string `json:"update_url,omitempty"`
}

// Command example
type Command struct {
	ID          *string      `json:"id"`
	CommandCode *int         `json:"command"`
	AngelboxIds []string     `json:"angelbox_ids"`
	Data        *CommandData `json:"data"`
	UserId      *string      `json:"user_id"`
	CreateTime  *time.Time   `json:"create_time"`
}

// CommandWrapper example
type CommandWrapper struct {
	*Command
	Raw_Data *string `json:"data,omitempty"`
}

// CommandReq example
type CommandReq struct {
	*Command
	Omit_ID         util.Omit `json:"id,omitempty"`
	Omit_CreateTime util.Omit `json:"create_time,omitempty"`
}

// Bind defines the pre-processing of JSON request body before converting to CommandReq.
func (body *CommandReq) Bind(req *http.Request) error {
	if req.Method == http.MethodPost {
		if body.CommandCode == nil {
			return errors.New("missing body parameter: <command>")
		}
		if body.AngelboxIds == nil {
			return errors.New("missing body parameter: <angelbox_ids>")
		} else if len(body.AngelboxIds) <= 0 {
			return errors.New("empty body parameter: <angelbox_ids>")
		}
	}
	return nil
}

// CommandRes example
type CommandRes struct {
	*Command
}

// Render defines the post-processing of CommandRes before converting to JSON response body.
func (body *CommandRes) Render(res http.ResponseWriter, req *http.Request) error {
	return nil
}

// NewCommandRes is the factory method for CommandRes.
func NewCommandRes(command *Command) *CommandRes {
	return &CommandRes{Command: command}
}

// NewCommandListRes is the factory method for an array of CommandRes.
func NewCommandListRes(commands []*Command) []render.Renderer {
	list := []render.Renderer{}
	for _, command := range commands {
		list = append(list, NewCommandRes(command))
	}
	return list
}

// CommandResponseBody example
type CommandResponseBody struct {
	Message *string `json:"msg,omitempty"`
	Error   *string `json:"err,omitempty"`
}

// CommandResponse example
type CommandResponse struct {
	AngelboxId *string              `json:"angelbox_id"`
	IsSuccess  *bool                `json:"is_success"`
	Response   *CommandResponseBody `json:"response"`
	RecordTime *time.Time           `json:"record_time"`
}

// CommandResponseWrapper example
type CommandResponseWrapper struct {
	*CommandResponse
	Raw_Response *string `json:"response,omitempty"`
}

// CommandResponseRes example
type CommandResponseRes struct {
	*CommandResponse
}

// Render defines the post-processing of CommandResponseRes before converting to JSON response body.
func (body *CommandResponseRes) Render(res http.ResponseWriter, req *http.Request) error {
	return nil
}

// NewCommandResponseRes is the factory method for CommandResponseRes.
func NewCommandResponseRes(commandResponse *CommandResponse) *CommandResponseRes {
	return &CommandResponseRes{CommandResponse: commandResponse}
}

// NewCommandResponseListRes is the factory method for an array of CommandResponseRes.
func NewCommandResponseListRes(commandResponses []*CommandResponse) []render.Renderer {
	list := []render.Renderer{}
	for _, commandResponse := range commandResponses {
		list = append(list, NewCommandResponseRes(commandResponse))
	}
	return list
}
