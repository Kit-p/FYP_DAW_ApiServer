// This file defines API handlers for the /commands/* APIs.

package command

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"

	firebase "firebase.google.com/go/v4"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	"daw/apiserver/pkg/backendworker"
	errorhandler "daw/apiserver/pkg/handler/error"
	"daw/apiserver/pkg/handler/user"
	"daw/apiserver/pkg/handler/util"
)

// Handler returns a sub router that is to be mounted to the main router.
// It defines all API endpoints under /angelboxes.
// For documentation of each handler, reference the godoc above the corresponding handler.
func Handler(app *firebase.App, dbPool *pgxpool.Pool, config *util.Config, worker *backendworker.BackendWorker) http.Handler {
	router := chi.NewRouter()

	// make all endpoints under /commands require a minimum role of OPERATOR
	router.Use(user.RBAC(dbPool, user.OPERATOR))

	router.Get("/", RenderAllCommands(dbPool))

	router.Post("/", CreateCommand(app, dbPool, worker))

	router.Route(fmt.Sprintf("/{%s}", ParamCommandId), func(r chi.Router) {
		r.Use(CommandLogContext(dbPool))

		r.Get("/", RenderCommand())

		r.Get("/response", RenderCommandResponses(dbPool))
		r.Get("/responses", RenderCommandResponses(dbPool))
	})

	return router
}

// RenderAllCommands godoc
// @Summary      Render all command history in the database
// @Description  Get all command history from the database and put their JSON representation into an array. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {array}   CommandRes           "All the command history in JSON format"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /command [get]
// @Router       /commands [get]
func RenderAllCommands(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		commands, err := dbGetCommands(req.Context(), dbPool, pgx.TxOptions{})
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		if err := render.RenderList(res, req, NewCommandListRes(commands)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// RenderCommand godoc
// @Summary      Render a command history
// @Description  Read a command history from the context value and render it as an JSON object. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {object}  CommandRes           "The command history in JSON format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing path parameter 'commandId'"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "Command history not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /command/{id} [get]
// @Router       /commands/{id} [get]
func RenderCommand() http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		command := req.Context().Value(COMMAND).(*Command)
		if command == nil {
			render.Render(res, req, errorhandler.ErrNotFound)
			return
		}

		if err := render.Render(res, req, NewCommandRes(command)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// RenderCommandResponses godoc
// @Summary      Render all command responses of a command in the database
// @Description  Get all command responses of a command from the database and put their JSON representation into an array. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {array}   CommandResponseRes   "All the command responses of a command in JSON format"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /command/{id}/response [get]
// @Router       /commands/{id}/responses [get]
func RenderCommandResponses(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		command := req.Context().Value(COMMAND).(*Command)
		if command == nil {
			render.Render(res, req, errorhandler.ErrNotFound)
			return
		}

		responses, err := dbGetCommandResponses(req.Context(), dbPool, pgx.TxOptions{}, *command.ID)
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		if err := render.RenderList(res, req, NewCommandResponseListRes(responses)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// CreateCommand godoc
// @Summary      Issue a command
// @Description  Issue a command to MQTT broker and create a command history in the database from an object of command details. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true   "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Param        command        body      int                  true   "The command code to be issued, corresponding to database mapping"
// @Param        angelbox_ids   body      []string             false  "The targeted AngelBox IDs in an array"
// @Param        data           body      CommandData          false  "The custom data associated with the command"
// @Success      201            {object}  CommandRes           "The created command history in JSON format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing / Invalid body parameter(s)"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "Command not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /command [post]
// @Router       /commands [post]
func CreateCommand(app *firebase.App, dbPool *pgxpool.Pool, worker *backendworker.BackendWorker) http.HandlerFunc {
	var fn http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
		ctx := req.Context()
		caller := ctx.Value(user.CALLER).(*user.User)

		data := &CommandReq{Command: &Command{}}
		if err := render.Bind(req, data); err != nil && err != io.EOF {
			render.Render(res, req, errorhandler.ErrInvalidRequest(err))
			return
		}
		data.UserId = caller.ID
		createTime := time.Now()
		data.CreateTime = &createTime

		if id, err := dbCreateCommand(req.Context(), dbPool, pgx.TxOptions{}, data.Command); err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		} else {
			data.ID = &id
		}

		var commandData *string = nil
		if data.Data != nil {
			if bytes, err := json.Marshal(data.Data); err != nil {
				render.Render(res, req, errorhandler.ErrInternal(err))
				return
			} else {
				str := string(bytes)
				commandData = &str
			}
		}
		if err := worker.PublishCommand(*data.ID, *data.CommandCode, data.AngelboxIds, commandData); err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		render.Status(req, http.StatusCreated)
		ctx = context.WithValue(req.Context(), COMMAND, data.Command)
		RenderCommand()(res, req.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}
