// This file contains API middlewares for the /commands/* APIs.

package command

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	errorhandler "daw/apiserver/pkg/handler/error"
)

type CommandContextKey string

const COMMAND CommandContextKey = "command"

var ParamCommandId string = "commandId"

// CommandLogContext queries the Command_Log table with id specified in the endpoint path parameter.
// The command log data is then available in the context with the COMMAND key.
func CommandLogContext(dbPool *pgxpool.Pool) func(http.Handler) http.Handler {
	fn := func(next http.Handler) http.Handler {
		var fn http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
			// read the path parameter
			commandId := chi.URLParam(req, ParamCommandId)
			if commandId == "" {
				render.Render(res, req,
					errorhandler.ErrInvalidRequest(fmt.Errorf("missing path parameter <%s>", ParamCommandId)))
				return
			}

			// retrieve data from the database
			if command, err := dbGetCommand(req.Context(), dbPool, pgx.TxOptions{}, commandId); err != nil {
				render.Render(res, req, errorhandler.ErrNotFound)
				return
			} else {
				// append data to the request context
				ctx := context.WithValue(req.Context(), COMMAND, command)
				// serve the request with the new context
				next.ServeHTTP(res, req.WithContext(ctx))
			}
		}
		return http.HandlerFunc(fn)
	}
	return fn
}
