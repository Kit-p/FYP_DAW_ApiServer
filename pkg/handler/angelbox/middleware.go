// This file contains API middlewares for the /angelboxes/* APIs.

package angelbox

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	errorhandler "daw/apiserver/pkg/handler/error"
)

type AngelboxContextKey string

const ANGELBOX AngelboxContextKey = "angelbox"

var ParamAngelboxId string = "angelboxId"

// AngelboxContext queries the AngelBox table with id specified in the endpoint path parameter.
// The AngelBox data is then available in the context with the ANGELBOX key.
func AngelboxContext(dbPool *pgxpool.Pool) func(http.Handler) http.Handler {
	fn := func(next http.Handler) http.Handler {
		var fn http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
			// read the path parameter
			angelboxId := chi.URLParam(req, ParamAngelboxId)
			if angelboxId == "" {
				render.Render(res, req,
					errorhandler.ErrInvalidRequest(fmt.Errorf("missing path parameter <%s>", ParamAngelboxId)))
				return
			}

			// retrieve data from the database
			if angelbox, err := dbGetAngelbox(req.Context(), dbPool, pgx.TxOptions{}, angelboxId); err != nil {
				render.Render(res, req, errorhandler.ErrNotFound)
				return
			} else {
				// append data to the request context
				ctx := context.WithValue(req.Context(), ANGELBOX, angelbox)
				// serve the request with the new context
				next.ServeHTTP(res, req.WithContext(ctx))
			}
		}
		return http.HandlerFunc(fn)
	}
	return fn
}

const ANGELBOXSTATUS AngelboxContextKey = "angelboxstatus"

// AngelboxStatusContext queries the AngelBox_Status table with id specified in the endpoint path parameter.
// The AngelBox status data is then available in the context with the ANGELBOXSTATUS key.
func AngelboxStatusContext(dbPool *pgxpool.Pool) func(http.Handler) http.Handler {
	fn := func(next http.Handler) http.Handler {
		var fn http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
			// read the path parameter
			angelboxId := chi.URLParam(req, ParamAngelboxId)
			if angelboxId == "" {
				render.Render(res, req,
					errorhandler.ErrInvalidRequest(fmt.Errorf("missing path parameter <%s>", ParamAngelboxId)))
				return
			}

			// retrieve data from the database
			if angelboxStatus, err := dbGetAngelboxStatus(req.Context(), dbPool, pgx.TxOptions{}, angelboxId); err != nil {
				render.Render(res, req, errorhandler.ErrNotFound)
				return
			} else {
				// append data to the request context
				ctx := context.WithValue(req.Context(), ANGELBOXSTATUS, angelboxStatus)
				// serve the request with the new context
				next.ServeHTTP(res, req.WithContext(ctx))
			}
		}
		return http.HandlerFunc(fn)
	}
	return fn
}
