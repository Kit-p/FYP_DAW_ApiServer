// This file contains types and corresponding JSON schemas used in /angelboxes/* and /setup APIs.

package angelbox

import (
	"errors"
	"net/http"
	"time"

	"github.com/go-chi/render"

	"daw/apiserver/pkg/handler/util"
)

// Angelbox example
type Angelbox struct {
	ID          *string    `json:"id"`
	Mac_Address *string    `json:"mac_address"`
	InitX       *float64   `json:"init_x"`
	InitY       *float64   `json:"init_y"`
	InitZ       *float64   `json:"init_z"`
	Location_EN *string    `json:"location_en"`
	Location_ZH *string    `json:"location_zh"`
	GPS         *bool      `json:"gps"`
	State       *string    `json:"state"`
	Version     *string    `json:"version"`
	CreateTime  *time.Time `json:"create_time"`
}

// OverrideWith updates the Angelbox with a possibly incomplete Angelbox object other.
// Validations and conditions are checked before updating the values.
func (angelbox *Angelbox) OverrideWith(other *Angelbox) error {
	if other == nil {
		return errors.New("parameter is nil")
	}
	if other.InitX != nil {
		angelbox.InitX = other.InitX
	}
	if other.InitY != nil {
		angelbox.InitY = other.InitY
	}
	if other.InitZ != nil {
		angelbox.InitZ = other.InitZ
	}
	if other.Location_EN != nil {
		angelbox.Location_EN = other.Location_EN
	}
	if other.Location_ZH != nil {
		angelbox.Location_ZH = other.Location_ZH
	}
	if other.GPS != nil {
		// if originally static and updates to mobile, nullify all initial coordinates
		if angelbox.GPS != nil && *angelbox.GPS != *other.GPS && *other.GPS {
			angelbox.InitX = nil
			angelbox.InitY = nil
			angelbox.InitZ = nil
		}
		angelbox.GPS = other.GPS
	}
	if other.State != nil {
		angelbox.State = other.State
	}

	if angelbox.ID == nil {
		angelbox.ID = other.ID
	}
	if angelbox.Mac_Address == nil {
		angelbox.Mac_Address = other.Mac_Address
	}
	if angelbox.Version == nil {
		angelbox.Version = other.Version
	}
	if angelbox.CreateTime == nil {
		angelbox.CreateTime = other.CreateTime
	}
	if angelbox.Mac_Address == nil {
		return errors.New("missing required field: <Mac_Address>")
	}
	if angelbox.Location_EN == nil {
		return errors.New("missing required field: <Location_EN>")
	}
	if angelbox.Location_ZH == nil {
		return errors.New("missing required field: <Location_ZH>")
	}
	if angelbox.GPS == nil {
		return errors.New("missing required field: <GPS>")
	}
	if angelbox.State == nil {
		return errors.New("missing required field: <State>")
	}

	// validate initial coordinates with static / mobile type
	initXNil, initYNil, initZNil := angelbox.InitX == nil, angelbox.InitY == nil, angelbox.InitZ == nil
	allNil := initXNil && initYNil && initZNil
	latlngNil := initXNil || initYNil
	if (*other.GPS && !allNil) || (!*other.GPS && latlngNil) {
		return errors.New("conflicting fields: <GPS>, <Init*>")
	}
	return nil
}

// AngelboxRegisterReq example
type AngelboxRegisterReq struct {
	Mac_Address *string `json:"mac_address"`
	Version     *string `json:"version"`
}

// Bind defines the pre-processing of JSON request body before converting to AngelboxRegisterReq.
func (body *AngelboxRegisterReq) Bind(req *http.Request) error {
	if req.Method == http.MethodPost {
		if body.Mac_Address == nil {
			return errors.New("missing body parameter: <mac_address>")
		}
		if body.Version == nil {
			return errors.New("missing body parameter: <version>")
		}
	}
	return nil
}

// AngelboxRegisterRes example
type AngelboxRegisterRes struct {
	ID string `json:"id"`
}

// Render defines the post-processing of AngelboxRegisterRes before converting to JSON response body.
func (body *AngelboxRegisterRes) Render(res http.ResponseWriter, req *http.Request) error {
	return nil
}

// NewAngelboxRegisterRes is the factory method for AngelboxRegisterRes.
func NewAngelboxRegisterRes(id string) *AngelboxRegisterRes {
	return &AngelboxRegisterRes{ID: id}
}

// AngelboxReq example
type AngelboxReq struct {
	*Angelbox
	Omit_ID         util.Omit `json:"id,omitempty"`          // id must be ignored for request
	Omit_CreateTime util.Omit `json:"create_time,omitempty"` // create_time must be ignored for request
}

// Bind defines the pre-processing of JSON request body before converting to AngelboxReq.
func (body *AngelboxReq) Bind(req *http.Request) error {
	if req.Method == http.MethodPost {
		if body.GPS == nil {
			return errors.New("missing body parameter: <gps>")
		}
		if body.State == nil {
			return errors.New("missing body parameter: <state>")
		}
		if body.Location_EN == nil || body.Location_ZH == nil {
			return errors.New("missing body parameters: <location_*>")
		}
		if *body.GPS != (body.InitX == nil || body.InitY == nil || body.InitZ == nil) {
			return errors.New("conflicting body parameters: <gps>, <init_*>")
		}
		if *body.Location_EN == "" || *body.Location_ZH == "" {
			return errors.New("empty body parameters: <location_*>")
		}
	}
	return nil
}

// AngelboxRes example
type AngelboxRes struct {
	*Angelbox
}

// Render defines the post-processing of AngelboxRes before converting to JSON response body.
func (body *AngelboxRes) Render(res http.ResponseWriter, req *http.Request) error {
	return nil
}

// NewAngelboxRes is the factory method for AngelboxRes.
func NewAngelboxRes(angelbox *Angelbox) *AngelboxRes {
	return &AngelboxRes{Angelbox: angelbox}
}

// NewAngelboxListRes is the factory method for an array of AngelboxRes.
func NewAngelboxListRes(angelboxes []*Angelbox) []render.Renderer {
	list := []render.Renderer{}
	for _, angelbox := range angelboxes {
		list = append(list, NewAngelboxRes(angelbox))
	}
	return list
}

// AngelboxStatus example
type AngelboxStatus struct {
	*Angelbox
	CurrentX   *float64   `json:"current_x"`
	CurrentY   *float64   `json:"current_y"`
	CurrentZ   *float64   `json:"current_z"`
	GpsError   *float64   `json:"gps_error"`
	GpsTime    *time.Time `json:"gps_time"`
	LastActive *time.Time `json:"last_active"`
	NumIssue   *int       `json:"num_issue"`
}

// AngelboxStatusRes example
type AngelboxStatusRes struct {
	*AngelboxStatus
}

// Render defines the post-processing of AngelboxStatusRes before converting to JSON response body.
func (body *AngelboxStatusRes) Render(res http.ResponseWriter, req *http.Request) error {
	return nil
}

// NewAngelboxStatusRes is the factory method for an array of AngelboxStatusRes.
func NewAngelboxStatusRes(angelboxStatus *AngelboxStatus) *AngelboxStatusRes {
	return &AngelboxStatusRes{AngelboxStatus: angelboxStatus}
}

// NewAngelboxStatusListRes is the factory method for an array of AngelboxStatusRes.
func NewAngelboxStatusListRes(angelboxesStatus []*AngelboxStatus) []render.Renderer {
	list := []render.Renderer{}
	for _, angelboxStatus := range angelboxesStatus {
		list = append(list, NewAngelboxStatusRes(angelboxStatus))
	}
	return list
}
