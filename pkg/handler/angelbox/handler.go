// This file defines API handlers for the /angelboxes/* and /setup APIs.

package angelbox

import (
	"context"
	"fmt"
	"net/http"
	"time"

	firebase "firebase.google.com/go/v4"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	errorhandler "daw/apiserver/pkg/handler/error"
	"daw/apiserver/pkg/handler/user"
	"daw/apiserver/pkg/handler/util"
)

// Handler returns a sub router that is to be mounted to the main router.
// It defines all API endpoints under /angelboxes.
// For documentation of each handler, reference the godoc above the corresponding handler.
func Handler(app *firebase.App, dbPool *pgxpool.Pool, config *util.Config) http.Handler {
	router := chi.NewRouter()

	// make all endpoints under /angelboxes require a minimum role of OPERATOR
	router.Use(user.RBAC(dbPool, user.OPERATOR))

	router.Get("/", RenderAllAngelboxes(dbPool))

	// disabled until the web portal implements a UI for this endpoint
	// router.Post("/", CreateAngelbox(dbPool))

	router.Get("/pending", RenderPendingAngelboxes(dbPool))

	router.Route("/status", func(r chi.Router) {
		r.Get("/", RenderAllAngelboxesStatus(dbPool))

		r.With(AngelboxStatusContext(dbPool)).Get(fmt.Sprintf("/{%s}", ParamAngelboxId), RenderAngelboxStatus())
	})

	router.Route(fmt.Sprintf("/{%s}", ParamAngelboxId), func(r chi.Router) {
		r.Use(AngelboxContext(dbPool))

		r.Get("/", RenderAngelbox())

		r.Put("/", UpdateAngelbox(dbPool))

		r.Delete("/", DeleteAngelbox(dbPool))
	})

	return router
}

// RenderAllAngelboxes godoc
// @Summary      Render all AngelBoxes in the database
// @Description  Get all AngelBoxes from the database and put their JSON representation into an array. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {array}   AngelboxRes          "All the AngelBoxes in JSON format"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /angelbox [get]
// @Router       /angelboxes [get]
func RenderAllAngelboxes(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		angelboxes, err := dbGetAngelboxes(req.Context(), dbPool, pgx.TxOptions{})
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		if err := render.RenderList(res, req, NewAngelboxListRes(angelboxes)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// RenderPendingAngelboxes godoc
// @Summary      Render all pending AngelBoxes in the database
// @Description  Get all pending AngelBoxes from the database and put their JSON representation into an array. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {array}   AngelboxRegisterRes  "All the pending AngelBoxes in JSON format"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /angelbox/pending [get]
// @Router       /angelboxes/pending [get]
func RenderPendingAngelboxes(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		angelboxes, err := dbGetPendingAngelboxes(req.Context(), dbPool, pgx.TxOptions{})
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		if err := render.RenderList(res, req, NewAngelboxListRes(angelboxes)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// RenderAngelbox godoc
// @Summary      Render an AngelBox
// @Description  Read an AngelBox from the context value and render it as an JSON object. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {object}  AngelboxRes          "The AngelBox in JSON format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing path parameter 'angelboxId'"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "AngelBox not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /angelbox/{id} [get]
// @Router       /angelboxes/{id} [get]
func RenderAngelbox() http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		angelbox := req.Context().Value(ANGELBOX).(*Angelbox)
		if angelbox == nil {
			render.Render(res, req, errorhandler.ErrNotFound)
			return
		}

		if err := render.Render(res, req, NewAngelboxRes(angelbox)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// RenderAllAngelboxesStatus godoc
// @Summary      Render all AngelBoxes with their latest status in the database
// @Description  Get latest status of all AngelBoxes from the database and put their JSON representation into an array. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {array}   AngelboxStatusRes    "All the AngelBoxes with their latest status in JSON format"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /angelbox/status [get]
// @Router       /angelboxes/status [get]
func RenderAllAngelboxesStatus(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		angelboxesStatus, err := dbGetAngelboxesStatus(req.Context(), dbPool, pgx.TxOptions{})
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		if err := render.RenderList(res, req, NewAngelboxStatusListRes(angelboxesStatus)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// RenderAngelboxStatus godoc
// @Summary      Render an AngelBox with its latest status
// @Description  Read an AngelBox Status from the context value and render it as an JSON object. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {object}  AngelboxStatusRes    "The AngelBox with its latest status in JSON format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing path parameter 'angelboxId'"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "AngelBox not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /angelbox/status/{id} [get]
// @Router       /angelboxes/status/{id} [get]
func RenderAngelboxStatus() http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		angelboxStatus := req.Context().Value(ANGELBOXSTATUS).(*AngelboxStatus)
		if angelboxStatus == nil {
			render.Render(res, req, errorhandler.ErrNotFound)
			return
		}

		if err := render.Render(res, req, NewAngelboxStatusRes(angelboxStatus)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// CreatePendingAngelbox godoc
// @Summary      Create a pending AngelBox in the database
// @Description  Create a pending AngelBox in the database from an object of AngelBox MAC address. Does not require any role.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Param        mac_address    body      string               true  "The MAC address of the AngelBox requesting setup"
// @Success      201            {object}  AngelboxRegisterRes  "The ID of the created AngelBox in JSON format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing body parameter(s)"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// @Router       /setup [post]
func CreatePendingAngelbox(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		ctx := req.Context()

		data := &AngelboxRegisterReq{}
		if err := render.Bind(req, data); err != nil {
			render.Render(res, req, errorhandler.ErrInvalidRequest(err))
			return
		}

		id, err := dbRegisterAngelbox(ctx, dbPool, pgx.TxOptions{}, data)
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		if err := render.Render(res, req, NewAngelboxRegisterRes(id)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// CreateAngelbox godoc
// @Summary      Create an AngelBox in the database
// @Description  Create an AngelBox in the database from an object of AngelBox details. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true   "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Param        init_x         body      float32              false  "The initial x-coordinate of the AngelBox to be created, required if 'gps' is false"
// @Param        init_y         body      float32              false  "The initial y-coordinate of the AngelBox to be created, required if 'gps' is false"
// @Param        init_z         body      float32              false  "The initial z-coordinate of the AngelBox to be created, required if 'gps' is false"
// @Param        location_en    body      string               true   "The location (in English text)  of  the  AngelBox  to  be  created"
// @Param        location_zh    body      string               true   "The location (in Chinese text)  of  the  AngelBox  to  be  created"
// @Param        gps            body      boolean              true   "Whether the AngelBox to be created is located by GPS signal"
// @Param        enabled        body      boolean              true   "Whether the AngelBox to be created is enabled"
// @Success      201            {object}  AngelboxRes          "The created AngelBox in JSON format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing body parameter(s)"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "AngelBox not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
//               // {Deprecated}  @Router  /angelbox [post]
//               // @Router       /angelboxes [post]
// @Deprecated
func CreateAngelbox(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		ctx := req.Context()

		data := &AngelboxReq{}
		if err := render.Bind(req, data); err != nil {
			render.Render(res, req, errorhandler.ErrInvalidRequest(err))
			return
		}
		createTime := time.Now()
		data.Angelbox.CreateTime = &createTime

		if id, err := dbCreateAngelbox(ctx, dbPool, pgx.TxOptions{}, data.Angelbox); err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		} else {
			data.Angelbox.ID = &id
		}
		render.Status(req, http.StatusCreated)
		ctx = context.WithValue(req.Context(), ANGELBOX, data.Angelbox)
		RenderAngelbox()(res, req.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}

// UpdateAngelbox godoc
// @Summary      Update an AngelBox in the database
// @Description  Update an AngelBox in the database from an object of AngelBox details (unchanged fields can be omitted). Requires role OPERATOR or above.
// @Param        Authorization  header    string               true                                          "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Param        init_x         body      float32              false                                         "The initial x-coordinate of the AngelBox to be created, required if 'gps' is false"
// @Param        init_y         body      float32              false                                         "The initial y-coordinate of the AngelBox to be created, required if 'gps' is false"
// @Param        init_z         body      float32              false                                         "The initial z-coordinate of the AngelBox to be created, required if 'gps' is false"
// @Param        location_en    body      string               false                                         "The location (in English text)  of  the  AngelBox  to  be  created"
// @Param        location_zh    body      string               false                                         "The location (in Chinese text)  of  the  AngelBox  to  be  created"
// @Param        gps            body      boolean              false                                         "Whether the AngelBox to be created is located by GPS signal"
// @Param        enabled        body      boolean              false                                         "Whether the AngelBox to be created is enabled"
// @Success      200            {object}  AngelboxRes          "The updated AngelBox (with updated details)  in  JSON  format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing body parameter(s)"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "AngelBox not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /angelbox/{id} [put]
// @Router       /angelboxes/{id} [put]
func UpdateAngelbox(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		ctx := req.Context()
		angelbox := req.Context().Value(ANGELBOX).(*Angelbox)
		if angelbox == nil {
			render.Render(res, req, errorhandler.ErrNotFound)
			return
		}

		data := &AngelboxReq{}
		if err := render.Bind(req, data); err != nil {
			render.Render(res, req, errorhandler.ErrInvalidRequest(err))
			return
		}

		angelbox.OverrideWith(data.Angelbox)

		if err := dbUpdateAngelbox(ctx, dbPool, pgx.TxOptions{}, angelbox); err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}
		render.Status(req, http.StatusCreated)
		ctx = context.WithValue(req.Context(), ANGELBOX, angelbox)
		RenderAngelbox()(res, req.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}

// DeleteAngelbox godoc
// @Summary      Delete an AngelBox from the database
// @Description  Delete an AngelBox from the database specified by AngelBox id. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true                                                       "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {object}  AngelboxRes          "The deleted AngelBox (with details just before deletion)  in  JSON  format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing path parameter 'angelboxId'"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "AngelBox not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /angelbox/{id} [delete]
// @Router       /angelboxes/{id} [delete]
func DeleteAngelbox(dbConn *pgxpool.Pool) http.HandlerFunc {
	var fn http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
		ctx := req.Context()

		angelbox := ctx.Value(ANGELBOX).(*Angelbox)

		if angelbox == nil || angelbox.ID == nil {
			render.Render(res, req, errorhandler.ErrNotFound)
			return
		}

		if err := dbDeleteAngelbox(req.Context(), dbConn, pgx.TxOptions{}, *angelbox.ID); err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		ctx = context.WithValue(ctx, ANGELBOX, angelbox)
		RenderAngelbox()(res, req.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}
