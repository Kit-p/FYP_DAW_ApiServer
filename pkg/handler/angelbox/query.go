// This file contains database queries used in handlers for the /angelboxes/* and /setup APIs.

package angelbox

import (
	"context"
	"errors"
	"fmt"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"

	"daw/apiserver/pkg/db"
)

var AngelboxTableName string = "AngelBox"

// dbGetAngelboxes is a package private method to query database for all AngelBoxes.
func dbGetAngelboxes(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions) ([]*Angelbox, error) {
	var angelboxes []*Angelbox
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT id, mac_address, init_x, init_y, init_z, location_en, location_zh, gps, state, version, create_time FROM %s.%s",
			db.DatabaseName,
			AngelboxTableName,
		)

		rows, err := tx.Query(context.Background(),
			sql,
		)

		for rows.Next() {
			var angelbox *Angelbox = &Angelbox{}
			rows.Scan(&angelbox.ID, &angelbox.Mac_Address, &angelbox.InitX, &angelbox.InitX, &angelbox.InitZ, &angelbox.Location_EN, &angelbox.Location_ZH, &angelbox.GPS, &angelbox.State, &angelbox.Version, &angelbox.CreateTime)
			angelboxes = append(angelboxes, angelbox)
		}

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return angelboxes, err
}

// dbGetPendingAngelboxes is a package private method to query database for all AngelBoxes with pending state.
func dbGetPendingAngelboxes(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions) ([]*Angelbox, error) {
	var angelboxes []*Angelbox
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT id, mac_address, init_x, init_y, init_z, location_en, location_zh, gps, state, version, create_time FROM %s.%s where state='pending'",
			db.DatabaseName,
			AngelboxTableName,
		)

		rows, err := tx.Query(context.Background(),
			sql,
		)

		for rows.Next() {
			var angelbox *Angelbox = &Angelbox{}
			rows.Scan(&angelbox.ID, &angelbox.Mac_Address, &angelbox.InitX, &angelbox.InitY, &angelbox.InitZ, &angelbox.Location_EN, &angelbox.Location_ZH, &angelbox.GPS, &angelbox.State, &angelbox.Version, &angelbox.CreateTime)
			angelboxes = append(angelboxes, angelbox)
		}

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return angelboxes, err
}

// dbGetAngelbox is a package private method to query database for an AngelBox with the specific angelboxId.
func dbGetAngelbox(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string) (*Angelbox, error) {
	var angelbox *Angelbox = &Angelbox{}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT id, mac_address, init_x, init_y, init_z, location_en, location_zh, gps, state, version, create_time FROM %s.%s WHERE id=$1",
			db.DatabaseName,
			AngelboxTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
			angelboxId,
		).Scan(&angelbox.ID, &angelbox.Mac_Address, &angelbox.InitX, &angelbox.InitY, &angelbox.InitZ, &angelbox.Location_EN, &angelbox.Location_ZH, &angelbox.GPS, &angelbox.State, &angelbox.Version, &angelbox.CreateTime)

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return angelbox, err
}

var AngelboxStatusTableName string = "AngelBox_Status"

// dbGetAngelboxesStatus is a package private method to query database for real-time status of all AngelBoxes.
func dbGetAngelboxesStatus(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions) ([]*AngelboxStatus, error) {
	var angelboxes []*AngelboxStatus
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT id, mac_address, init_x, init_y, init_z, location_en, location_zh, gps, state, version, create_time, current_x, current_y, current_z, gps_error, gps_time, last_active, num_issue FROM %s.%s",
			db.DatabaseName,
			AngelboxStatusTableName,
		)

		rows, err := tx.Query(context.Background(),
			sql,
		)

		for rows.Next() {
			var angelbox *AngelboxStatus = &AngelboxStatus{Angelbox: &Angelbox{}}
			rows.Scan(&angelbox.ID, &angelbox.Mac_Address, &angelbox.InitX, &angelbox.InitY, &angelbox.InitZ, &angelbox.Location_EN, &angelbox.Location_ZH, &angelbox.GPS, &angelbox.State, &angelbox.Version, &angelbox.CreateTime, &angelbox.CurrentX, &angelbox.CurrentY, &angelbox.CurrentZ, &angelbox.GpsError, &angelbox.GpsTime, &angelbox.LastActive, &angelbox.NumIssue)
			angelboxes = append(angelboxes, angelbox)
		}

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return angelboxes, err
}

// dbGetAngelboxStatus is a package private method to query database for the real-time status of an AngelBox with the specific angelboxId.
func dbGetAngelboxStatus(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string) (*AngelboxStatus, error) {
	var angelbox *AngelboxStatus = &AngelboxStatus{Angelbox: &Angelbox{}}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT id, mac_address, init_x, init_y, init_z, location_en, location_zh, gps, state, version, create_time, current_x, current_y, current_z, gps_error, gps_time, last_active, num_issue FROM %s.%s WHERE id=$1",
			db.DatabaseName,
			AngelboxStatusTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
			angelboxId,
		).Scan(&angelbox.ID, &angelbox.Mac_Address, &angelbox.InitX, &angelbox.InitY, &angelbox.InitZ, &angelbox.Location_EN, &angelbox.Location_ZH, &angelbox.GPS, &angelbox.State, &angelbox.Version, &angelbox.CreateTime, &angelbox.CurrentX, &angelbox.CurrentY, &angelbox.CurrentZ, &angelbox.GpsError, &angelbox.GpsTime, &angelbox.LastActive, &angelbox.NumIssue)

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return angelbox, err
}

// dbRegisterAngelbox is a package private method to insert into database a new AngelBox with pending state using data in request.
func dbRegisterAngelbox(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, request *AngelboxRegisterReq) (id string, err error) {
	id, err = "", nil
	if request == nil {
		return "", errors.New("parameter is nil")
	}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"INSERT INTO %s.%s (mac_address, version, state) VALUES ($1, $2, 'pending') RETURNING id",
			db.DatabaseName,
			AngelboxTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
			request.Mac_Address,
			request.Version,
		).Scan(&id)
		return err
	}
	err = crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return id, err
}

// dbCreateAngelbox is a package private method to insert into database a new AngelBox using data in angelbox.
func dbCreateAngelbox(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelbox *Angelbox) (id string, err error) {
	id, err = "", nil
	if angelbox == nil {
		return "", errors.New("parameter is nil")
	}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"INSERT INTO %s.%s (gps, init_x, init_y, init_z, location_en, location_zh, state) VALUES ($1, $2, $3, $4, $5 COLLATE en, $6 COLLATE zh, $7) RETURNING id",
			db.DatabaseName,
			AngelboxTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
			angelbox.GPS,
			angelbox.InitX,
			angelbox.InitY,
			angelbox.InitZ,
			angelbox.Location_EN,
			angelbox.Location_ZH,
			angelbox.State,
		).Scan(&id)
		return err
	}
	err = crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return id, err
}

// dbUpdateAngelbox is a package private method to update database an AngelBox using data in angelbox.
func dbUpdateAngelbox(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelbox *Angelbox) error {
	if angelbox == nil {
		return errors.New("parameter is nil")
	}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"UPDATE %s.%s SET gps=$2, init_x=$3, init_y=$4, init_z=$5, location_en=$6, location_zh=$7, state=$8 WHERE id=$1",
			db.DatabaseName,
			AngelboxTableName,
		)
		_, err := tx.Exec(context.Background(),
			sql,
			angelbox.ID,
			angelbox.GPS,
			angelbox.InitX,
			angelbox.InitY,
			angelbox.InitZ,
			angelbox.Location_EN,
			angelbox.Location_ZH,
			angelbox.State,
		)
		return err
	}
	return crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
}

// dbDeleteAngelbox is a package private method to delete database an AngelBox with the specific angelboxId.
func dbDeleteAngelbox(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string) error {
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"DELETE FROM %s.%s WHERE id=$1",
			db.DatabaseName,
			AngelboxTableName,
		)
		_, err := tx.Exec(context.Background(),
			sql,
			angelboxId,
		)
		return err
	}
	return crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
}
