// This file defines API handlers for the /summary/* APIs.

package summary

import (
	"net/http"

	errorhandler "daw/apiserver/pkg/handler/error"

	firebase "firebase.google.com/go/v4"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	"daw/apiserver/pkg/handler/user"
	"daw/apiserver/pkg/handler/util"
)

// Handler returns a sub router that is to be mounted to the main router.
// It defines all API endpoints under /summary.
// For documentation of each handler, reference the godoc above the corresponding handler.
func Handler(app *firebase.App, dbPool *pgxpool.Pool, config *util.Config) http.Handler {
	router := chi.NewRouter()

	// make all endpoints under /summary require a minimum role of OPERATOR
	router.Use(user.RBAC(dbPool, user.OPERATOR))

	router.Get("/", RenderSummary(dbPool))

	return router
}

// RenderSummary godoc
// @Summary      Render dashboard summary data gathered from the database
// @Description  Get number of enabled AngelBoxes, number of issue groups, number of CMS users, latest 5 issue groups and latest 5 command logs from the database and put their JSON representation into an array. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {object}  SummaryRes           "All the dashboard summary data in JSON format"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// @Router       /summary [get]
func RenderSummary(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		var summary *Summary = &Summary{}

		numBox, err := dbGetNumEnabledAngelbox(req.Context(), dbPool, pgx.TxOptions{})
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}
		summary.NumEnabledBox = numBox

		numIssue, err := dbGetNumGroupedIssue(req.Context(), dbPool, pgx.TxOptions{})
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}
		summary.NumUnresolvedIssue = numIssue

		numUser, err := dbGetNumUser(req.Context(), dbPool, pgx.TxOptions{})
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}
		summary.NumUser = numUser

		issuesGroups, err := dbGetLastFiveIssueGroup(req.Context(), dbPool, pgx.TxOptions{})
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}
		summary.IssueGroups = issuesGroups

		commands, err := dbGetLastFiveCommands(req.Context(), dbPool, pgx.TxOptions{})
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}
		summary.Commands = commands

		if err := render.Render(res, req, NewSummaryRes(summary)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}
