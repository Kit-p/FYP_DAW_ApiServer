// This file contains database queries used in handlers for the /summary/* APIs.

package summary

import (
	"context"
	"fmt"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"

	"daw/apiserver/pkg/db"
	"daw/apiserver/pkg/handler/command"
	"daw/apiserver/pkg/handler/issue"
)

var AngelboxTableName string = "AngelBox"
var BeaconScanTableName string = "Scan_Result"
var CommandTableName string = "Command_Log"
var IssueTableName string = "Issue"
var UserTableName string = "User"

// dbGetNumEnabledAngelbox is a package private method to query database for the number of enabled AngelBoxes.
func dbGetNumEnabledAngelbox(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions) (*int, error) {
	var numEnabledBox *int
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"select count(*) from %s.%s where state='enabled'::angelbox_state",
			db.DatabaseName,
			AngelboxTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
		).Scan(&numEnabledBox)

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return numEnabledBox, err
}

// dbGetNumGroupedIssue is a package private method to query database for the number of issue groups.
func dbGetNumGroupedIssue(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions) (*int, error) {
	var numGroupedIssue *int
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"select count(distinct group_id) from %s.%s where resolved=false",
			db.DatabaseName,
			IssueTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
		).Scan(&numGroupedIssue)

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return numGroupedIssue, err
}

// dbGetNumUser is a package private method to query database for the number of web portal users.
func dbGetNumUser(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions) (*int, error) {
	var numUser *int
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"select count(*) from %s.%s",
			db.DatabaseName,
			UserTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
		).Scan(&numUser)

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return numUser, err
}

// dbGetLastFiveIssueGroup is a package private method to query database for most recent five issue groups.
func dbGetLastFiveIssueGroup(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions) ([]*issue.IssueGroup, error) {
	var issueGroups = []*issue.IssueGroup{}

	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"with n as (select group_id, max(create_time) as last_occurrence, count(*) as num_occurrence from %s.%s group by group_id) select i.group_id, i.type, i.angelbox_id, i.error_code, i.detail_en, i.detail_zh, i.resolved, i.resolved_time, i.resolved_manually, n.last_occurrence, n.num_occurrence from %s.%s i inner join n on i.group_id=n.group_id and i.create_time=n.last_occurrence order by resolved asc, last_occurrence desc limit 5",
			db.DatabaseName,
			IssueTableName,
			db.DatabaseName,
			IssueTableName,
		)

		rows, err := tx.Query(context.Background(),
			sql,
		)

		for rows.Next() {
			var issueGroup *issue.IssueGroup = &issue.IssueGroup{}
			rows.Scan(&issueGroup.GroupId, &issueGroup.Type, &issueGroup.AngelboxId, &issueGroup.ErrorCode, &issueGroup.Detail_EN, &issueGroup.Detail_ZH, &issueGroup.Resolved, &issueGroup.ResolvedTime, &issueGroup.ResolvedManually, &issueGroup.LastOccurrence, &issueGroup.NumOccurrence)
			issueGroups = append(issueGroups, issueGroup)
		}

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return issueGroups, err
}

// dbGetLastFiveCommands is a package private method to query database for most recent five command logs.
func dbGetLastFiveCommands(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions) ([]*command.Command, error) {
	var commands = []*command.Command{}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT id, command, angelbox_ids, user_id, create_time FROM %s.%s order by create_time desc limit 5",
			db.DatabaseName,
			CommandTableName,
		)

		rows, err := tx.Query(context.Background(),
			sql,
		)

		for rows.Next() {
			var command *command.Command = &command.Command{}
			rows.Scan(&command.ID, &command.CommandCode, &command.AngelboxIds, &command.UserId, &command.CreateTime)
			commands = append(commands, command)
		}

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return commands, err
}
