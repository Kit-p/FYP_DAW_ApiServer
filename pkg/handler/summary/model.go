// This file contains types and corresponding JSON schemas used in /summary/* APIs.

package summary

import (
	"daw/apiserver/pkg/handler/command"
	"daw/apiserver/pkg/handler/issue"
	"net/http"
)

// Summary example
type Summary struct {
	NumEnabledBox      *int                `json:"num_enabled_box"`
	NumUnresolvedIssue *int                `json:"num_unresolved_issue"`
	NumUser            *int                `json:"num_user"`
	IssueGroups        []*issue.IssueGroup `json:"issues"`
	Commands           []*command.Command  `json:"commands"`
}

// SummaryRes example
type SummaryRes struct {
	*Summary
}

// Render defines the post-processing of SummaryRes before converting to JSON response body.
func (body *SummaryRes) Render(res http.ResponseWriter, req *http.Request) error {
	return nil
}

// NewSummaryRes is the factory method for SummaryRes.
func NewSummaryRes(summary *Summary) *SummaryRes {
	return &SummaryRes{Summary: summary}
}
