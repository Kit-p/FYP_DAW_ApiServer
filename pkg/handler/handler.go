// This file defines the main router for all API endpoints and sub routers.

package handler

import (
	"net/http"

	firebase "firebase.google.com/go/v4"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"github.com/go-chi/render"
	"github.com/jackc/pgx/v4/pgxpool"
	httpSwagger "github.com/swaggo/http-swagger"

	_ "daw/apiserver/docs"
	"daw/apiserver/pkg/backendworker"
	"daw/apiserver/pkg/handler/angelbox"
	"daw/apiserver/pkg/handler/auth"
	"daw/apiserver/pkg/handler/beaconscan"
	"daw/apiserver/pkg/handler/command"
	"daw/apiserver/pkg/handler/errorscan"
	"daw/apiserver/pkg/handler/heartbeat"
	"daw/apiserver/pkg/handler/issue"
	"daw/apiserver/pkg/handler/summary"
	"daw/apiserver/pkg/handler/user"
	"daw/apiserver/pkg/handler/util"
)

// Handler returns the main router that is to be mounted to the HTTP server.
// It defines all API endpoints under / and all sub routers.
// For documentation of each handler, reference the godoc above the corresponding handler.
func Handler(app *firebase.App, dbPool *pgxpool.Pool, config *util.Config, worker *backendworker.BackendWorker) http.Handler {
	// @Title        FYP2122_GCH2/DAW API
	// @Version      1.0
	// @Description  These APIs are expected to be called from the web portal application only.

	// @contact.name   PANG, Kit
	// @contact.email  kpangaa@connect.ust.hk

	// @BasePath  /
	// @Accept    json,x-www-form-urlencoded
	// @Produce   json,plain

	// @SecurityDefinitions.basic  BasicAuth

	// @Security  BasicAuth

	router := chi.NewRouter()

	// health check endpoint
	router.Use(middleware.Heartbeat("/health"))

	// CORS options
	// * Allow all origins for now
	router.Use(cors.Handler(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE"},
		AllowedHeaders: []string{"*"},
		// ExposedHeaders: []string{"*"},
		AllowCredentials: true,
		MaxAge:           3600,
	}))

	// shared middlewares for all endpoints
	router.Use(middleware.RequestID)
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)
	router.Use(render.SetContentType(render.ContentTypeJSON))

	// redirects to Swagger UI
	router.Get("/", RedirectToDocs)

	// serve Swagger UI
	router.Get("/docs/*", httpSwagger.WrapHandler)

	// AngelBox registration endpoint, exclude from RBAC
	router.Post("/setup", angelbox.CreatePendingAngelbox(dbPool))

	// web portal user registration endpoint, exclude from RBAC
	router.Post("/signup", user.CreateSelf(app, dbPool, config.AdministratorEmails))

	// web portal user self info endpoint, requires role TEMPORARY or above.
	router.With(user.RBAC(dbPool, user.TEMPORARY)).Get("/me", user.RenderSelf())
	// disabled until the web portal implements a UI for this endpoint
	// router.With(user.RBAC(dbPool, user.OPERATOR)).Put("/me", user.UpdateSelf(app, dbPool))
	// disabled until the web portal implements a UI for this endpoint
	// router.With(user.RBAC(app, dbPool, user.TEMPORARY)).Delete("/me", user.DeleteSelf(app, dbPool))

	// delegate path "/auth" to sub-router
	router.Mount("/auth", auth.Handler(app, dbPool, config))

	// delegate paths "/user" and "/users" to sub-router
	router.Mount("/user", user.Handler(app, dbPool, config))
	router.Mount("/users", user.Handler(app, dbPool, config))

	// delegate paths "/angelbox" and "/angelboxes" to sub-router
	router.Mount("/angelbox", angelbox.Handler(app, dbPool, config))
	router.Mount("/angelboxes", angelbox.Handler(app, dbPool, config))

	// delegate paths "/command" and "/commands" to sub-router
	router.Mount("/command", command.Handler(app, dbPool, config, worker))
	router.Mount("/commands", command.Handler(app, dbPool, config, worker))

	// delegate path "/issue" and "/issues" to sub-router
	router.Mount("/issue", issue.Handler(app, dbPool, config))
	router.Mount("/issues", issue.Handler(app, dbPool, config))

	// delegate path "/errorscan" and "/errorscans" to sub-router
	router.Mount("/errorscan", errorscan.Handler(app, dbPool, config))
	router.Mount("/errorscans", errorscan.Handler(app, dbPool, config))

	// delegate path "/heartbeat" and "/heartbeats" to sub-router
	router.Mount("/heartbeat", heartbeat.Handler(app, dbPool, config))
	router.Mount("/heartbeats", heartbeat.Handler(app, dbPool, config))

	// delegate path "/beaconscan" and "/beaconscans" to sub-router
	router.Mount("/beaconscan", beaconscan.Handler(app, dbPool, config))
	router.Mount("/beaconscans", beaconscan.Handler(app, dbPool, config))

	// delegate path "/summary" to sub-router
	router.Mount("/summary", summary.Handler(app, dbPool, config))

	return router
}

// RedirectToDocs godoc
// @Summary      The root path, redirects to API documentations
// @Description  Redirects to `/docs/index.html` upon GET request
// @Success      301
// @Router       / [get]
var RedirectToDocs http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
	http.Redirect(res, req, "/docs/index.html", http.StatusMovedPermanently)
}
