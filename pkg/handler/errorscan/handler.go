// This file defines API handlers for the /errorscans/* APIs.

package errorscan

import (
	"daw/apiserver/pkg/handler/user"
	"daw/apiserver/pkg/handler/util"
	"net/http"

	errorhandler "daw/apiserver/pkg/handler/error"

	firebase "firebase.google.com/go/v4"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler returns a sub router that is to be mounted to the main router.
// It defines all API endpoints under /errorscans.
// For documentation of each handler, reference the godoc above the corresponding handler.
func Handler(app *firebase.App, dbPool *pgxpool.Pool, config *util.Config) http.Handler {
	router := chi.NewRouter()

	// make all endpoints under /errorscans require a minimum role of OPERATOR
	router.Use(user.RBAC(dbPool, user.OPERATOR))

	router.Get("/", RenderAllErrorScanResults(dbPool))

	router.Route("/setting", func(r chi.Router) {
		r.Get("/", RenderErrorScanSetting(dbPool))
		r.Put("/", UpdateErrorScanSetting(dbPool))
	})

	return router
}

// RenderAllErrorScanResults godoc
// @Summary      Render all error scan results in the database
// @Description  Get all error scan results from the database and put their JSON representation into an array. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {array}   ErrorScanResultRes   "All the error scan results in JSON format"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /errorscan [get]
// @Router       /errorscans [get]
func RenderAllErrorScanResults(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		results, err := dbGetAllErrorScanResults(req.Context(), dbPool, pgx.TxOptions{})
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		if err := render.RenderList(res, req, NewErrorScanResultListRes(results)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// RenderErrorScanSetting godoc
// @Summary      Render all error scan settings in the database
// @Description  Get all error scan settings from the database and put their JSON representation into an array. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {object}  ErrorScanSettingRes  "All the error scan settings in JSON format"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /errorscan/setting [get]
// @Router       /errorscans/setting [get]
func RenderErrorScanSetting(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		var setting *ErrorScanSetting = &ErrorScanSetting{}

		scanInterval, err := dbGetSingleErrorScanSettingValue(req.Context(), dbPool, pgx.TxOptions{}, ErrorScanIntervalKey)
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}
		setting.ErrorScanInterval = scanInterval

		maxNotActive, err := dbGetSingleErrorScanSettingValue(req.Context(), dbPool, pgx.TxOptions{}, MaxNotActiveTimeKey)
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}
		setting.MaxNotActiveTime = maxNotActive

		maxNoGpsUpdate, err := dbGetSingleErrorScanSettingValue(req.Context(), dbPool, pgx.TxOptions{}, MaxGpsNoUpdateTimeKey)
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}
		setting.MaxGpsNoUpdateTime = maxNoGpsUpdate

		if err := render.Render(res, req, NewErrorScanSettingRes(setting)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// UpdateErrorScanSetting godoc
// @Summary      Update error scan settings in the database
// @Description  Update error scan settings in the database from an object of settings details. Requires role OPERATOR or above.
// @Param        Authorization           header    string               true                                                     "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Param        error_scan_interval     body      int                  false                                                    "The new error scan interval in minutes (1-3600)"
// @Param        max_not_active_time     body      int                  false                                                    "The new maximum AngelBox not active time in minutes (1-3600)"
// @Param        max_gps_no_update_time  body      int                  false                                                    "The new maximum AngelBox no gps update time in minutes (1-3600)"
// @Success      200                     {object}  ErrorScanSettingRes  "The updated error scan settings (with updated details)  in  JSON  format"
// @Failure      400                     {object}  errorhandler.ErrRes  "Missing body parameter(s)"
// @Failure      401                     {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403                     {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      422                     {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500                     {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /errorscan/setting [put]
// @Router       /errorscans/setting [put]
func UpdateErrorScanSetting(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		data := &ErrorScanSetting{}
		if err := render.Bind(req, data); err != nil {
			render.Render(res, req, errorhandler.ErrInvalidRequest(err))
			return
		}

		if err := dbUpdateErrorScanSetting(req.Context(), dbPool, pgx.TxOptions{}, data); err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		if err := render.Render(res, req, NewErrorScanSettingRes(data)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}
