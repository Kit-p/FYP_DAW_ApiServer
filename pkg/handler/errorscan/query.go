// This file contains database queries used in handlers for the /errorscans/* APIs.

package errorscan

import (
	"context"
	"daw/apiserver/pkg/db"
	"fmt"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"
)

var ErrorScanResultTableName string = "Error_Scan_Result"
var ServerSettingTableName string = "Error_Scan_Setting"

// dbGetAllErrorScanResults is a package private method to query database for all error scan results.
func dbGetAllErrorScanResults(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions) ([]*ErrorScanResult, error) {
	var errorScanResults []*ErrorScanResult

	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT id, success, start_time, end_time, num_not_active_box, num_box_with_issue, num_unresolved_issue, log, record_time FROM %s.%s order by record_time desc",
			db.DatabaseName,
			ErrorScanResultTableName,
		)

		rows, err := tx.Query(context.Background(),
			sql,
		)

		for rows.Next() {
			var errorScanResult *ErrorScanResult = &ErrorScanResult{}
			rows.Scan(&errorScanResult.ID, &errorScanResult.Success, &errorScanResult.StartTime, &errorScanResult.EndTime, &errorScanResult.NumNotActiveBox, &errorScanResult.NumBoxWithIssue, &errorScanResult.NumUnresolvedIssue, &errorScanResult.Log, &errorScanResult.RecordTime)
			errorScanResults = append(errorScanResults, errorScanResult)
		}

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return errorScanResults, err
}

// dbGetSingleErrorScanSettingValue is a package private method to query database for an error scan setting with the specific key.
func dbGetSingleErrorScanSettingValue(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, key string) (*int, error) {
	var settingValue *int
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT value FROM %s.%s WHERE key=$1 limit 1",
			db.DatabaseName,
			ServerSettingTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
			key,
		).Scan(&settingValue)

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return settingValue, err
}

// dbUpdateErrorScanSetting is a package private method to update database an error scan setting using data in setting.
func dbUpdateErrorScanSetting(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, setting *ErrorScanSetting) error {
	fn := func(tx pgx.Tx) error {
		batch := &pgx.Batch{}
		numInsert := 3
		updateSettingSql := fmt.Sprintf("UPDATE %s.%s SET value=$1 WHERE key=$2",
			db.DatabaseName,
			ServerSettingTableName,
		)

		batch.Queue(updateSettingSql, setting.ErrorScanInterval, ErrorScanIntervalKey)
		batch.Queue(updateSettingSql, setting.MaxNotActiveTime, MaxNotActiveTimeKey)
		batch.Queue(updateSettingSql, setting.MaxGpsNoUpdateTime, MaxGpsNoUpdateTimeKey)

		br := tx.SendBatch(context.Background(), batch)

		defer br.Close()

		for i := 0; i < numInsert; i++ {
			_, err := br.Exec()
			if err != nil {
				return err
			}
		}

		return nil
	}
	return crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
}
