// This file contains types and corresponding JSON schemas used in /errorscans/* APIs.

package errorscan

import (
	"errors"
	"net/http"
	"time"

	"github.com/go-chi/render"
)

var ErrorScanIntervalKey string = "error_scan_interval"
var MaxNotActiveTimeKey string = "max_not_active_time"
var MaxGpsNoUpdateTimeKey string = "max_gps_no_update_time"

// ErrorScanResult example
type ErrorScanResult struct {
	ID                 *string    `json:"id"`
	Success            *bool      `json:"success"`
	StartTime          *time.Time `json:"start_time"`
	EndTime            *time.Time `json:"end_time"`
	NumNotActiveBox    *int       `json:"num_not_active_box"`
	NumBoxWithIssue    *int       `json:"num_box_with_issue"`
	NumUnresolvedIssue *int       `json:"num_unresolved_issue"`
	Log                []string   `json:"log"`
	RecordTime         *time.Time `json:"record_time"`
}

// ErrorScanResultRes example
type ErrorScanResultRes struct {
	*ErrorScanResult
}

// Render defines the post-processing of ErrorScanResultRes before converting to JSON response body.
func (body *ErrorScanResultRes) Render(res http.ResponseWriter, req *http.Request) error {
	return nil
}

// NewErrorScanResultRes is the factory method for ErrorScanResultRes.
func NewErrorScanResultRes(result *ErrorScanResult) *ErrorScanResultRes {
	return &ErrorScanResultRes{ErrorScanResult: result}
}

// NewErrorScanResultListRes is the factory method for an array of ErrorScanResultRes.
func NewErrorScanResultListRes(results []*ErrorScanResult) []render.Renderer {
	list := []render.Renderer{}
	for _, result := range results {
		list = append(list, NewErrorScanResultRes(result))
	}
	return list
}

// ErrorScanSetting example
type ErrorScanSetting struct {
	ErrorScanInterval  *int `json:"error_scan_interval"`
	MaxNotActiveTime   *int `json:"max_not_active_time"`
	MaxGpsNoUpdateTime *int `json:"max_gps_no_update_time"`
}

// Bind defines the pre-processing of JSON request body before converting to ErrorScanSetting.
func (body *ErrorScanSetting) Bind(req *http.Request) error {
	if req.Method == http.MethodPut {
		if body.ErrorScanInterval == nil {
			return errors.New("missing body parameter: <error_scan_interval>")
		}
		if body.MaxNotActiveTime == nil {
			return errors.New("missing body parameter: <max_not_active_time>")
		}
		if body.MaxGpsNoUpdateTime == nil {
			return errors.New("missing body parameters: <max_gps_no_update_time>")
		}

		if *body.ErrorScanInterval < 1 || *body.MaxNotActiveTime < 1 || *body.MaxGpsNoUpdateTime < 1 {
			return errors.New("non postive setting value(s): <error_scan_interval>, <max_not_active_time>, <max_gps_no_update_time>")
		}
	}
	return nil
}

// ErrorScanSettingRes example
type ErrorScanSettingRes struct {
	*ErrorScanSetting
}

// Render defines the post-processing of ErrorScanSettingRes before converting to JSON response body.
func (body *ErrorScanSettingRes) Render(res http.ResponseWriter, req *http.Request) error {
	return nil
}

// NewErrorScanSettingRes is the factory method for ErrorScanSettingRes.
func NewErrorScanSettingRes(setting *ErrorScanSetting) *ErrorScanSettingRes {
	return &ErrorScanSettingRes{ErrorScanSetting: setting}
}
