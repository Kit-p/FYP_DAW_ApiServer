// This file defines utility functions regarding user authentication with Firebase admin SDK.

package util

import (
	"context"

	firebase "firebase.google.com/go/v4"
	"firebase.google.com/go/v4/auth"
)

// getFirebaseClient returns an instance of the authentication client from the Firebase application.
func getFirebaseClient(ctx context.Context, app *firebase.App) (success bool, client *auth.Client, err error) {
	success, client, err = false, nil, nil
	client, err = app.Auth(ctx)
	if err != nil {
		return
	}
	success = true
	return
}

// AuthorizeFirebaseUser authenticates a user with the specific idToken with the Firebase authentication client.
func AuthorizeFirebaseUser(ctx context.Context, app *firebase.App, idToken string) (success bool, token *auth.Token, err error) {
	var client *auth.Client
	token = nil
	success, client, err = getFirebaseClient(ctx, app)
	if !success {
		return
	}

	token, err = client.VerifyIDTokenAndCheckRevoked(ctx, idToken)
	return
}

// GetFirebaseUser returns the user data stored in Firebase with the specific uid.
func GetFirebaseUser(ctx context.Context, app *firebase.App, uid string) (success bool, user *auth.UserRecord, err error) {
	var client *auth.Client
	user = nil
	success, client, err = getFirebaseClient(ctx, app)
	if !success {
		return
	}

	user, err = client.GetUser(ctx, uid)
	return
}

// UpdateFirebaseUser updates the user data stored in Firebase with the specific uid using data in userToUpdate.
func UpdateFirebaseUser(ctx context.Context, app *firebase.App, uid string, userToUpdate *auth.UserToUpdate) (success bool, user *auth.UserRecord, err error) {
	var client *auth.Client
	user = nil
	success, client, err = getFirebaseClient(ctx, app)
	if !success {
		return
	}

	user, err = client.UpdateUser(ctx, uid, userToUpdate)
	return
}

// DeleteFirebaseUser deletes the user from Firebase with the specific uid.
func DeleteFirebaseUser(ctx context.Context, app *firebase.App, uid string) (success bool, err error) {
	var client *auth.Client
	success, client, err = getFirebaseClient(ctx, app)
	if !success {
		return
	}

	err = client.DeleteUser(ctx, uid)
	return
}
