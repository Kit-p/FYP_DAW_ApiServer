// This file defines utility functions regarding reading the configuration items from the config file or environmental variables.

package util

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"
	"time"
)

// GetEnv reads the environmental variable with the specific key and returns value of fallback if not found.
func GetEnv(key string, fallback string) string {
	if value, found := os.LookupEnv(key); found {
		return value
	}
	return fallback
}

type Environment string

// Environment constants
const (
	DEVELOPMENT Environment = "development"
	PRODUCTION  Environment = "production"
)

// Config example
type Config struct {
	Environment                           Environment `json:"environment"`
	ListenAddress                         string      `json:"listen_address"`
	DatabaseConnectionString              string      `json:"database_connection_string"`
	MqttBrokerAddress                     string      `json:"mqtt_broker_address"`
	AppServerAPIEndpoint                  string      `json:"app_server_api_endpoint"`
	FirebaseServiceAccountCredentialsPath string      `json:"firebase_service_account_credentials_path"`
	AdministratorEmails                   []string    `json:"administrator_emails"`
	ApiAuthTokenExpiryDuration            *Duration   `json:"api_auth_token_expiry_duration"`
}

// ParseConfig reads the config file and converts the JSON file content to Config.
func ParseConfig(filepath string) (*Config, error) {
	// read the file content from the specific filepath
	bytes, err := ioutil.ReadFile(filepath)
	if err != nil {
		return nil, err
	}

	// convert to Config object
	config := &Config{}
	if err := json.Unmarshal(bytes, config); err != nil {
		return nil, err
	}

	if string(config.Environment) == "" {
		config.Environment = PRODUCTION
	}

	if config.ListenAddress == "" {
		if config.Environment == DEVELOPMENT {
			config.ListenAddress = "0.0.0.0:8080"
		} else {
			config.ListenAddress = "0.0.0.0:18443"
		}
	}
	if config.DatabaseConnectionString == "" {
		if config.Environment == DEVELOPMENT {
			config.DatabaseConnectionString = "postgresql://root@localhost:26257?sslmode=disable"
		} else {
			return config, errors.New("missing config field <database_connection_string>")
		}
	}
	if config.MqttBrokerAddress == "" {
		if config.Environment == DEVELOPMENT {
			config.MqttBrokerAddress = "127.0.0.1:1883"
		} else {
			return config, errors.New("missing config field <mqtt_broker_address>")
		}
	}
	if config.AppServerAPIEndpoint == "" {
		if config.Environment == DEVELOPMENT {
			config.AppServerAPIEndpoint = "https://jccpauatbackend.azurefd.net/ibeacons/package"
		} else {
			return config, errors.New("missing config field <app_server_api_endpoint>")
		}
	}
	if config.FirebaseServiceAccountCredentialsPath == "" {
		return config, errors.New("missing config field <firebase_service_account_credentials_path>")
	}
	if config.ApiAuthTokenExpiryDuration == nil || config.ApiAuthTokenExpiryDuration.Duration < time.Minute {
		config.ApiAuthTokenExpiryDuration = &Duration{Duration: time.Hour * 4}
	}

	return config, nil
}
