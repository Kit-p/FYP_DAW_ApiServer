// This file contains database queries used in handlers for the /heartbeats/* APIs.

package heartbeat

import (
	"context"
	"daw/apiserver/pkg/db"
	"fmt"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"
)

var HeartbeatTableName string = "Heartbeat"

// dbGetAngelboxHeartbeats is a package private method to query database for all heartbeats of an AngelBox with the specific angelboxId.
func dbGetAngelboxHeartbeats(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string) ([]*Heartbeat, error) {
	var heartbeats []*Heartbeat

	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT id, angelbox_id, current_x, current_y, current_z, gps_error, gps_time, error_codes, record_time, insert_time FROM %s.%s where angelbox_id=$1 order by record_time desc limit 100",
			db.DatabaseName,
			HeartbeatTableName,
		)

		rows, err := tx.Query(context.Background(),
			sql,
			angelboxId,
		)

		for rows.Next() {
			var heartbeat *Heartbeat = &Heartbeat{}
			rows.Scan(&heartbeat.ID, &heartbeat.AngelboxId, &heartbeat.CurrentX, &heartbeat.CurrentY, &heartbeat.CurrentZ, &heartbeat.GpsError, &heartbeat.GpsTime, &heartbeat.ErrorCodes, &heartbeat.RecordTime, &heartbeat.InsertTime)
			heartbeats = append(heartbeats, heartbeat)
		}

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return heartbeats, err
}
