// This file contains types and corresponding JSON schemas used in /heartbeats/* APIs.

package heartbeat

import (
	"net/http"
	"time"

	"github.com/go-chi/render"
)

// Heartbeat example
type Heartbeat struct {
	ID         *string    `json:"id"`
	AngelboxId *string    `json:"angelbox_id"`
	CurrentX   *float64   `json:"current_x"`
	CurrentY   *float64   `json:"current_y"`
	CurrentZ   *float64   `json:"current_z"`
	GpsError   *float64   `json:"gps_error"`
	GpsTime    *time.Time `json:"gps_time"`
	ErrorCodes []int      `json:"error_codes"`
	RecordTime *time.Time `json:"record_time"`
	InsertTime *time.Time `json:"insert_time"`
}

// HeartbeatRes example
type HeartbeatRes struct {
	*Heartbeat
}

// Render defines the post-processing of HeartbeatRes before converting to JSON response body.
func (body *HeartbeatRes) Render(res http.ResponseWriter, req *http.Request) error {
	return nil
}

// NewHeartbeatRes is the factory method for HeartbeatRes.
func NewHeartbeatRes(result *Heartbeat) *HeartbeatRes {
	return &HeartbeatRes{Heartbeat: result}
}

// NewHeartbeatListRes is the factory method for an array of HeartbeatRes.
func NewHeartbeatListRes(heartbeats []*Heartbeat) []render.Renderer {
	list := []render.Renderer{}
	for _, heartbeat := range heartbeats {
		list = append(list, NewHeartbeatRes(heartbeat))
	}
	return list
}
