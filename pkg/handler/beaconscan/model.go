// This file contains types and corresponding JSON schemas used in /beaconscans/* APIs.

package beaconscan

import (
	"net/http"
	"time"

	"github.com/go-chi/render"
)

// Location example
type Location struct {
	LocationX *float64 `json:"location_x"`
	LocationY *float64 `json:"location_y"`
}

// LocationRes example
type LocationRes struct {
	*Location
}

// Render defines the post-processing of LocationRes before converting to JSON response body.
func (body *LocationRes) Render(res http.ResponseWriter, req *http.Request) error {
	return nil
}

// NewLocationRes is the factory method for LocationRes.
func NewLocationRes(location *Location) *LocationRes {
	return &LocationRes{Location: location}
}

// NewLocationListRes is the factory method for an array of LocationRes.
func NewLocationListRes(locations []*Location) []render.Renderer {
	list := []render.Renderer{}
	for _, location := range locations {
		list = append(list, NewLocationRes(location))
	}
	return list
}

// BeaconScan example
type BeaconScan struct {
	AngelboxId *string    `json:"angelbox_id"`
	MacAddress *string    `json:"mac_address"`
	BeaconUUID *string    `json:"beacon_uuid"`
	Major      *int       `json:"major"`
	Minor      *int       `json:"minor"`
	RSSI       *int       `json:"rssi"`
	LocationX  *float64   `json:"location_x"`
	LocationY  *float64   `json:"location_y"`
	LocationZ  *float64   `json:"location_z"`
	GpsError   *float64   `json:"gps_error"`
	GpsTime    *time.Time `json:"gps_time"`
	RecordTime *time.Time `json:"record_time"`
	InsertTime *time.Time `json:"insert_time"`
}

// BeaconScanRes example
type BeaconScanRes struct {
	*BeaconScan
}

// Render defines the post-processing of BeaconScanRes before converting to JSON response body.
func (body *BeaconScanRes) Render(res http.ResponseWriter, req *http.Request) error {
	return nil
}

// NewBeaconScanRes is the factory method for BeaconScanRes.
func NewBeaconScanRes(scan *BeaconScan) *BeaconScanRes {
	return &BeaconScanRes{BeaconScan: scan}
}

// NewBeaconScanListRes is the factory method for an array of BeaconScanRes.
func NewBeaconScanListRes(scans []*BeaconScan) []render.Renderer {
	list := []render.Renderer{}
	for _, scan := range scans {
		list = append(list, NewBeaconScanRes(scan))
	}
	return list
}
