// This file defines API handlers for the /beaconscans/* APIs.

package beaconscan

import (
	"daw/apiserver/pkg/handler/angelbox"
	"daw/apiserver/pkg/handler/user"
	"daw/apiserver/pkg/handler/util"
	"fmt"
	"net/http"

	errorhandler "daw/apiserver/pkg/handler/error"

	firebase "firebase.google.com/go/v4"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

// Handler returns a sub router that is to be mounted to the main router.
// It defines all API endpoints under /beaconscans.
// For documentation of each handler, reference the godoc above the corresponding handler.
func Handler(app *firebase.App, dbPool *pgxpool.Pool, config *util.Config) http.Handler {
	router := chi.NewRouter()

	// make all endpoints under /beaconscans require a minimum role of OPERATOR
	router.Use(user.RBAC(dbPool, user.OPERATOR))

	router.Get("/location", RenderAllBeaconScanLocations(dbPool))
	router.Get("/locations", RenderAllBeaconScanLocations(dbPool))

	router.Route(fmt.Sprintf("/angelbox/{%s}", angelbox.ParamAngelboxId), func(r chi.Router) {
		r.Use(angelbox.AngelboxContext(dbPool))

		r.Get("/", RenderAngelboxBeaconScans(dbPool))
	})

	return router
}

// RenderAllBeaconScanLocations godoc
// @Summary      Render all beacon scan locations in the database
// @Description  Get all beacon scan locations from the database and put their JSON representation into an array. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {array}   LocationRes          "All the beacon scan locations in JSON format"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /beaconscan/location [get]
// @Router       /beaconscans/locations [get]
func RenderAllBeaconScanLocations(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		locations, err := dbGetAllBeaconScanLocations(req.Context(), dbPool, pgx.TxOptions{})
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		if err := render.RenderList(res, req, NewLocationListRes(locations)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// RenderAngelboxBeaconScans godoc
// @Summary      Render all beacon scan results (limited to latest 100 rows) of an AngelBox in the database
// @Description  Get all (limited to latest 100 rows) beacon scan results of an AngelBox from the database and put their JSON representation into an array. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {array}   BeaconScanRes        "All the beacon scan results of an AngelBox in JSON format"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "AngelBox not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /beaconscan/angelbox/{id} [get]
// @Router       /beaconscans/angelbox/{id} [get]
func RenderAngelboxBeaconScans(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		angelbox := req.Context().Value(angelbox.ANGELBOX).(*angelbox.Angelbox)
		if angelbox == nil {
			render.Render(res, req, errorhandler.ErrNotFound)
			return
		}

		scans, err := dbGetAngelboxBeaconScans(req.Context(), dbPool, pgx.TxOptions{}, *angelbox.ID)
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		if err := render.RenderList(res, req, NewBeaconScanListRes(scans)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}
