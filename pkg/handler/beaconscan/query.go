// This file contains database queries used in handlers for the /beaconscans/* APIs.

package beaconscan

import (
	"context"
	"daw/apiserver/pkg/db"
	"fmt"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"
)

var BeaconScanTableName string = "Scan_Result"

// dbGetAllBeaconScanLocations is a package private method to query database for all coordinates of beacon scan results.
func dbGetAllBeaconScanLocations(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions) ([]*Location, error) {
	var locations []*Location

	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT location_x, location_y FROM %s.%s where location_x is not null and location_y is not null and gps_error is not null and gps_time is not null order by angelbox_id asc, record_time desc",
			db.DatabaseName,
			BeaconScanTableName,
		)

		rows, err := tx.Query(context.Background(),
			sql,
		)

		for rows.Next() {
			var location *Location = &Location{}
			rows.Scan(&location.LocationX, &location.LocationY)
			locations = append(locations, location)
		}
		rows.Close()

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return locations, err
}

// dbGetAngelboxBeaconScans is a package private method to query database for all beacon scan results of an AngelBox with the specific angelboxId.
func dbGetAngelboxBeaconScans(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string) ([]*BeaconScan, error) {
	var scans []*BeaconScan

	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT angelbox_id, mac_address, beacon_uuid, major, minor, rssi, location_x, location_y, location_z, gps_error, gps_time, record_time, insert_time FROM %s.%s where angelbox_id=$1 order by record_time desc limit 100",
			db.DatabaseName,
			BeaconScanTableName,
		)

		rows, err := tx.Query(context.Background(),
			sql,
			angelboxId,
		)

		for rows.Next() {
			var scan *BeaconScan = &BeaconScan{}
			rows.Scan(&scan.AngelboxId, &scan.MacAddress, &scan.BeaconUUID, &scan.Major, &scan.Minor, &scan.RSSI, &scan.LocationX, &scan.LocationY, &scan.LocationZ, &scan.GpsError, &scan.GpsTime, &scan.RecordTime, &scan.InsertTime)
			scans = append(scans, scan)
		}

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return scans, err
}
