// This file defines API handlers for the /auth/* APIs.

package auth

import (
	"fmt"
	"net/http"
	"time"

	firebase "firebase.google.com/go/v4"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	errorhandler "daw/apiserver/pkg/handler/error"
	"daw/apiserver/pkg/handler/util"
)

// Handler returns a sub router that is to be mounted to the main router.
// It defines all API endpoints under /auth.
// For documentation of each handler, reference the godoc above the corresponding handler.
func Handler(app *firebase.App, dbPool *pgxpool.Pool, config *util.Config) http.Handler {
	router := chi.NewRouter()

	router.Post("/", AuthUser(app, dbPool, config.ApiAuthTokenExpiryDuration.Duration))

	return router
}

// AuthUser godoc
// @Summary      Authenticate a user and return an auth token
// @Description  Authenticate a user by Firebase and generate an auth token with expiry time for subsequent API calls. Does not require any role.
// @Param        Authorization  header    string               true  "Firebase ID Token of API caller for identification, format ':\<idToken\>', in base64"
// @Success      200            {object}  AuthTokenRes         "The generated auth token in JSON format"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing ':\<idToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "User not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /auth [post]
// @Router       /auth [post]
func AuthUser(app *firebase.App, dbPool *pgxpool.Pool, expiryDuration time.Duration) http.HandlerFunc {
	var fn http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
		ctx := req.Context()

		// read Firebase ID token from Authorization header
		_, idToken, success := req.BasicAuth()
		if !success || idToken == "" {
			render.Render(res, req,
				errorhandler.ErrInvalidRequest(fmt.Errorf("missing authorization header ':<%s>'[base64]", "id_token")))
			return
		}

		success, token, err := util.AuthorizeFirebaseUser(ctx, app, idToken)
		if !success {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}
		if err != nil {
			render.Render(res, req, errorhandler.ErrUnauthorized(err))
			return
		}

		// check if there is existing not expired auth token for the user
		authToken, err := dbGetUserAuthToken(ctx, dbPool, pgx.TxOptions{}, token.UID)
		if authToken == nil || authToken.Token == nil || authToken.ExpiryTime == nil || err != nil {
			// generate and record the new auth token for the user
			authToken = NewAuthToken(token.UID, expiryDuration)
			if err := dbUpsertUserAuthToken(ctx, dbPool, pgx.TxOptions{}, authToken); err != nil {
				render.Render(res, req, errorhandler.ErrInternal(err))
				return
			}
		}

		if err := render.Render(res, req, NewAuthTokenRes(authToken)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}
