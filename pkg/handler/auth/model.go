// This file contains types and corresponding JSON schemas used in /auth/* APIs.

package auth

import (
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"net/http"
	"time"

	"daw/apiserver/pkg/handler/util"
)

// AuthToken example
type AuthToken struct {
	UserId     *string    `json:"user_id"`
	Token      *string    `json:"token"`
	ExpiryTime *time.Time `json:"expiry_time"`
}

// NewAuthToken is the factory method for AuthToken.
func NewAuthToken(userId string, expiryDuration time.Duration) *AuthToken {
	expiryTime := time.Now().Add(expiryDuration)

	// use "userId@currentTimeString" as the token content
	seed := fmt.Sprintf("%s@%s", userId, time.Now().UTC().String())
	// hash the content with SHA-256 algorithm
	bytes := sha256.Sum256([]byte(seed))
	// make the hash a string
	hash := hex.EncodeToString(bytes[:])

	return &AuthToken{UserId: &userId, Token: &hash, ExpiryTime: &expiryTime}
}

// AuthTokenRes example
type AuthTokenRes struct {
	*AuthToken
	Protected_UserId util.Omit `json:"user_id,omitempty"`
}

// Render defines the post-processing of AuthTokenRes before converting to JSON response body.
func (body *AuthTokenRes) Render(res http.ResponseWriter, req *http.Request) error {
	return nil
}

// NewAuthTokenRes is the factory method for AuthTokenRes.
func NewAuthTokenRes(authToken *AuthToken) *AuthTokenRes {
	return &AuthTokenRes{AuthToken: authToken}
}
