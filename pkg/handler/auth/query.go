// This file contains database queries used in handlers for the /auth/* APIs.

package auth

import (
	"context"
	"daw/apiserver/pkg/db"
	"errors"
	"fmt"
	"time"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"
)

var UserTokenTableName string = "User_Token"

// dbGetUserAuthToken is a package private method to query database for the auth token of a user with the specific userId.
func dbGetUserAuthToken(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, userId string) (*AuthToken, error) {
	var authToken *AuthToken = &AuthToken{}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT user_id, token, expiry_time FROM %s.%s WHERE user_id=$1 AND expiry_time>$2",
			db.DatabaseName,
			UserTokenTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
			userId,
			time.Now(),
		).Scan(&authToken.UserId, &authToken.Token, &authToken.ExpiryTime)

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return authToken, err
}

// dbUpsertUserAuthToken is a package private method to update or insert into database a new auth token for a user using data in authToken.
func dbUpsertUserAuthToken(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, authToken *AuthToken) error {
	if authToken == nil || authToken.UserId == nil {
		return errors.New("parameter is nil")
	}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"UPSERT INTO %s.%s (user_id, token, expiry_time) VALUES ($1, $2, $3)",
			db.DatabaseName,
			UserTokenTableName,
		)
		_, err := tx.Exec(context.Background(),
			sql,
			authToken.UserId,
			authToken.Token,
			authToken.ExpiryTime,
		)
		return err
	}
	return crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
}
