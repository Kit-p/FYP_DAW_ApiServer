// This file contains types and corresponding JSON schemas used in /users/* APIs and all APIs with RBAC.

package user

import (
	"daw/apiserver/pkg/handler/util"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/render"
)

type UserRole string

// UserRole constants
const (
	GUEST         UserRole = "guest" // constant for no role
	TEMPORARY     UserRole = "temporary"
	OPERATOR      UserRole = "operator"
	ADMINISTRATOR UserRole = "administrator"
)

// StrToUserRole converts a string to UserRole constants, GUEST if unknown string is supplied.
func StrToUserRole(role string) *UserRole {
	var r UserRole
	switch role {
	case string(TEMPORARY):
		r = TEMPORARY
	case string(OPERATOR):
		r = OPERATOR
	case string(ADMINISTRATOR):
		r = ADMINISTRATOR
	default:
		r = GUEST
	}
	return &r
}

// CanAccess checks if a certain UserRole constant has access to a specific role requirement.
func (r *UserRole) CanAccess(minRole UserRole) bool {
	if *r == minRole {
		return true
	}

	switch minRole {
	case ADMINISTRATOR:
		return false
	case OPERATOR:
		return *r == ADMINISTRATOR
	case TEMPORARY:
		return *r != GUEST
	default:
		// minRole == GUEST || minRole == nil
		return true
	}
}

// User example
type User struct {
	ID         *string    `json:"id"`
	Role       *UserRole  `json:"role"`
	Name       *string    `json:"name"`
	Email      *string    `json:"email"`
	Enabled    *bool      `json:"enabled"`
	CreateTime *time.Time `json:"create_time"`
}

// OverrideWith updates the user with a possibly incomplete User object other.
// Validations and conditions are checked before updating the values.
func (user *User) OverrideWith(other *User) error {
	if other == nil {
		return errors.New("parameter is nil")
	}
	if other.Role != nil {
		user.Role = other.Role
	}
	if other.Name != nil {
		user.Name = other.Name
	}
	if other.Email != nil {
		user.Email = other.Email
	}
	if other.Enabled != nil {
		user.Enabled = other.Enabled
	}

	if user.ID == nil {
		user.ID = other.ID
	}
	if user.CreateTime == nil {
		user.CreateTime = other.CreateTime
	}
	if user.ID == nil {
		return errors.New("missing required field: <ID>")
	}
	if user.Role == nil {
		return errors.New("missing required field: <Role>")
	}
	if user.Enabled == nil {
		return errors.New("missing required field: <Enabled>")
	}

	return nil
}

// UserReq example
type UserReq struct {
	*User
	Raw_ID          *string   `json:"id,omitempty"`
	Raw_Role        *string   `json:"role,omitempty"`
	Omit_CreateTime util.Omit `json:"create_time,omitempty"`
}

// Bind defines the pre-processing of JSON request body before converting to UserReq.
func (body *UserReq) Bind(req *http.Request) error {
	if req.Method == http.MethodPost {
		if body.Raw_ID == nil {
			return fmt.Errorf("missing body parameter <%s>", ParamUserId)
		}
		body.User.ID = body.Raw_ID
	}
	if body.Raw_Role != nil {
		body.User.Role = StrToUserRole(*body.Raw_Role)
	}
	return nil
}

// UserRes example
type UserRes struct {
	*User
	Protected_ID *string `json:"id,omitempty"`
}

// Render defines the post-processing of UserRes before converting to JSON response body.
func (body *UserRes) Render(res http.ResponseWriter, req *http.Request) error {
	caller := req.Context().Value(CALLER).(*User)
	if caller != nil && *caller.Role == ADMINISTRATOR {
		body.Protected_ID = body.ID
	}
	return nil
}

// NewUserRes is the factory method for UserRes.
func NewUserRes(user *User) *UserRes {
	return &UserRes{User: user}
}

// NewUserListRes is the factory method for an array of UserRes.
func NewUserListRes(users []*User) []render.Renderer {
	list := []render.Renderer{}
	for _, user := range users {
		list = append(list, NewUserRes(user))
	}
	return list
}
