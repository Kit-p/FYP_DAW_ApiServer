// This file defines API handlers for the /users/*, /me and /signup APIs.

package user

import (
	"context"
	"fmt"
	"io"
	"net/http"
	"time"

	firebase "firebase.google.com/go/v4"
	"firebase.google.com/go/v4/auth"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	errorhandler "daw/apiserver/pkg/handler/error"
	"daw/apiserver/pkg/handler/util"
)

// Handler returns a sub router that is to be mounted to the main router.
// It defines all API endpoints under /users.
// For documentation of each handler, reference the godoc above the corresponding handler.
func Handler(app *firebase.App, dbPool *pgxpool.Pool, config *util.Config) http.Handler {
	router := chi.NewRouter()

	// make all endpoints under /users require a minimum role of ADMINISTRATOR
	router.Use(RBAC(dbPool, ADMINISTRATOR))

	router.Get("/", RenderAllUsers(dbPool))

	// disabled until the web portal implements a UI for this endpoint
	// router.Post("/", CreateUser(app, dbPool, config.AdministratorEmails))

	router.Route(fmt.Sprintf("/{%s}", ParamUserId), func(r chi.Router) {
		r.Use(UserContext(dbPool))

		r.Get("/", RenderUser())

		r.Put("/", UpdateUser(app, dbPool))

		r.Delete("/", DeleteUser(app, dbPool))
	})

	return router
}

// RenderAllUsers godoc
// @Summary      Render all users in the database
// @Description  Get all users from the database and put their JSON representation into an array. Requires role ADMINISTRATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {array}   UserRes              "All the users in JSON format"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /user [get]
// @Router       /users [get]
func RenderAllUsers(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		users, err := dbGetUsers(req.Context(), dbPool, pgx.TxOptions{})
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		if err := render.RenderList(res, req, NewUserListRes(users)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// renderUser reads the user data from the context and return it as a JSON object.
func renderUser(ctxKey interface{}) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		user := req.Context().Value(ctxKey).(*User)
		if user == nil {
			render.Render(res, req, errorhandler.ErrNotFound)
			return
		}

		if err := render.Render(res, req, NewUserRes(user)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// RenderUser godoc
// @Summary      Render a user
// @Description  Read a user from the context value and render it as an JSON object. Requires role ADMINISTRATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {object}  UserRes              "The user in JSON format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing path parameter 'userId'"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "User not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /user/{id} [get]
// @Router       /users/{id} [get]
func RenderUser() http.HandlerFunc {
	return renderUser(USER)
}

// RenderSelf godoc
// @Summary      Render the current user
// @Description  Read the current user (i.e., the caller) from the context value and render it as an JSON object. Requires role TEMPORARY or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {object}  UserRes              "The user in JSON format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing path parameter 'userId'"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "User not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// @Router       /me [get]
func RenderSelf() http.HandlerFunc {
	return renderUser(CALLER)
}

// CreateUser godoc
// @Summary      Create a user in the database
// @Description  Create a user in the database from an object of Firebase user details. Requires role ADMINISTRATOR or above.
// @Param        Authorization  header    string               true   "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Param        userId         body      string               false  "The user id (from Firebase)  of  the  user  to  be  created"
// @Param        role           body      string               false  "The role to be assigned to the user to be created"
// @Success      201            {object}  UserRes              "The created user in JSON format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing body parameter(s)"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "User not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
//               // {Deprecated}  @Router  /user [post]
//               // @Router       /users [post]
// @Deprecated
func CreateUser(app *firebase.App, dbPool *pgxpool.Pool, adminEmails []string) http.HandlerFunc {
	var fn http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
		ctx := req.Context()
		caller := ctx.Value(CALLER).(*User)

		data := &UserReq{User: &User{}}
		if err := render.Bind(req, data); err != nil && err != io.EOF {
			render.Render(res, req, errorhandler.ErrInvalidRequest(err))
			return
		}

		if data.User.ID != nil && *data.User.ID != "" {
			success, firebaseUser, err := util.GetFirebaseUser(ctx, app, *data.User.ID)
			if !success {
				render.Render(res, req, errorhandler.ErrInternal(err))
				return
			}
			if err != nil {
				render.Render(res, req, errorhandler.ErrInvalidRequest(err))
				return
			}

			data.User.ID = &firebaseUser.UID
			data.User.Name = &firebaseUser.DisplayName
			data.User.Email = &firebaseUser.Email
			enabled := !firebaseUser.Disabled
			data.User.Enabled = &enabled
			createTime := time.Now()
			data.User.CreateTime = &createTime

			if !caller.Role.CanAccess(ADMINISTRATOR) {
				*data.User.Role = TEMPORARY
			}
		} else {
			render.Render(res, req,
				errorhandler.ErrInvalidRequest(fmt.Errorf("missing body parameter <%s>", ParamUserId)))
			return
		}

		if *data.User.Role == GUEST {
			*data.User.Role = TEMPORARY
		}

		if err := dbCreateUser(req.Context(), dbPool, pgx.TxOptions{}, data.User); err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		render.Status(req, http.StatusCreated)
		ctx = context.WithValue(req.Context(), USER, data.User)
		RenderUser()(res, req.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}

// CreateSelf godoc
// @Summary      Create the current user in the database
// @Description  Create the current user in the database using the idToken from the Authorization header. Requires role ADMINISTRATOR or above.
// @Param        Authorization  header    string               true  "Firebase ID Token of API caller for identification, format ':\<idToken\>', in base64"
// @Success      201            {object}  UserRes              "The created user in JSON format"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// @Router       /signup [post]
func CreateSelf(app *firebase.App, dbPool *pgxpool.Pool, adminEmails []string) http.HandlerFunc {
	var fn http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
		ctx := req.Context()

		_, idToken, success := req.BasicAuth()
		if !success || idToken == "" {
			render.Render(res, req,
				errorhandler.ErrInvalidRequest(fmt.Errorf("missing authorization header ':<%s>'[base64]", "id_token")))
			return
		}

		success, token, err := util.AuthorizeFirebaseUser(ctx, app, idToken)
		if !success {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}
		if err != nil {
			render.Render(res, req, errorhandler.ErrUnauthorized(err))
			return
		}

		success, firebaseUser, err := util.GetFirebaseUser(ctx, app, token.UID)
		if !success {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}
		if err != nil {
			render.Render(res, req, errorhandler.ErrInvalidRequest(err))
			return
		}

		var user *User = &User{}
		user.ID = &firebaseUser.UID
		user.Name = &firebaseUser.DisplayName
		user.Email = &firebaseUser.Email
		role := TEMPORARY
		user.Role = &role
		enabled := !firebaseUser.Disabled
		user.Enabled = &enabled
		createTime := time.Now()
		user.CreateTime = &createTime

		if user.Email != nil && util.StringSliceContains(adminEmails, *user.Email) {
			*user.Role = ADMINISTRATOR
		}

		if err := dbCreateUser(req.Context(), dbPool, pgx.TxOptions{}, user); err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		render.Status(req, http.StatusCreated)
		ctx = context.WithValue(req.Context(), USER, user)
		ctx = context.WithValue(ctx, CALLER, user)
		RenderUser()(res, req.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}

// updateUser reads the user data from the context and return it as a JSON object.
func updateUser(ctxKey interface{}, app *firebase.App, dbPool *pgxpool.Pool) http.HandlerFunc {
	var fn http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
		ctx := req.Context()

		user := ctx.Value(ctxKey).(*User)

		if user == nil {
			render.Render(res, req, errorhandler.ErrNotFound)
			return
		}

		data := &UserReq{}
		if err := render.Bind(req, data); err != nil {
			render.Render(res, req, errorhandler.ErrInvalidRequest(err))
			return
		}

		success, firebaseUser, err := util.GetFirebaseUser(ctx, app, *user.ID)
		if !success || err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		hasParams := false
		firebaseParams := &auth.UserToUpdate{}
		if data.User.Name != nil && *data.User.Name != firebaseUser.DisplayName {
			firebaseParams.DisplayName(*data.User.Name)
			hasParams = true
		}
		if data.User.Enabled != nil && *data.User.Enabled == firebaseUser.Disabled {
			firebaseParams.Disabled(!*data.User.Enabled)
			hasParams = true
		}
		if hasParams {
			success, firebaseUser, err = util.UpdateFirebaseUser(ctx, app, *user.ID, firebaseParams)
			if !success || err != nil {
				render.Render(res, req, errorhandler.ErrInternal(err))
				return
			}
		}

		data.User.ID = &firebaseUser.UID
		data.User.Name = &firebaseUser.DisplayName
		data.User.Email = &firebaseUser.Email
		enabled := !firebaseUser.Disabled
		data.User.Enabled = &enabled

		user.OverrideWith(data.User)

		if err := dbUpdateUser(req.Context(), dbPool, pgx.TxOptions{}, user); err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		ctx = context.WithValue(ctx, USER, user)
		RenderUser()(res, req.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}

// UpdateUser godoc
// @Summary      Update a user in the database and Firebase
// @Description  Update a user in the database and Firebase with role (optional) and Firebase user details. Requires role ADMINISTRATOR or above.
// @Param        Authorization  header    string               true                                      "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Param        role           body      string               false                                     "The new role of the user"
// @Success      200            {object}  UserRes              "The updated user (with updated details)  in  JSON  format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing body parameter(s)"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "User not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /user/{id} [put]
// @Router       /users/{id} [put]
func UpdateUser(app *firebase.App, dbPool *pgxpool.Pool) http.HandlerFunc {
	return updateUser(USER, app, dbPool)
}

// UpdateSelf godoc
// @Summary      Update the current user in the database and Firebase
// @Description  Update the current user in the database and Firebase with role (optional) and Firebase user details. Requires role TEMPORARY or above (ADMINISTRATOR or above for updating role).
// @Param        Authorization  header    string               true                                      "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Param        role           body      string               false                                     "The new role of the user"
// @Param        name           body      string               false                                     "The new name of the user"
// @Param        enabled        body      bool                 false                                     "Whether the user should be enabled"
// @Success      200            {object}  UserRes              "The updated user (with updated details)  in  JSON  format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing body parameter(s)"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "User not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
//               // @Router       /me [put]
// @Deprecated
func UpdateSelf(app *firebase.App, dbPool *pgxpool.Pool) http.HandlerFunc {
	return updateUser(CALLER, app, dbPool)
}

// deleteUser reads the user data from the context and return it as a JSON object.
func deleteUser(ctxKey interface{}, app *firebase.App, dbPool *pgxpool.Pool) http.HandlerFunc {
	var fn http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
		ctx := req.Context()

		user := ctx.Value(ctxKey).(*User)

		if user == nil {
			render.Render(res, req, errorhandler.ErrNotFound)
			return
		}

		if success, err := util.DeleteFirebaseUser(ctx, app, *user.ID); !success || err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		if err := dbDeleteUser(req.Context(), dbPool, pgx.TxOptions{}, *user.ID); err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		ctx = context.WithValue(ctx, USER, user)
		RenderUser()(res, req.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}

// DeleteUser godoc
// @Summary      Delete a user from the database and Firebase
// @Description  Delete a user from the database and Firebase specified by user id. Requires role ADMINISTRATOR or above.
// @Param        Authorization  header    string               true                                                   "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {object}  UserRes              "The deleted user (with details just before deletion)  in  JSON  format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing path parameter 'userId'"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "User not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /user/{id} [delete]
// @Router       /users/{id} [delete]
func DeleteUser(app *firebase.App, dbPool *pgxpool.Pool) http.HandlerFunc {
	return deleteUser(USER, app, dbPool)
}

// DeleteSelf godoc
// @Summary      Delete the current user from the database and Firebase
// @Description  Delete the current user from the database and Firebase. Requires role TEMPORARY or above.
// @Param        Authorization  header    string               true                                                   "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {object}  UserRes              "The deleted user (with details just before deletion)  in  JSON  format"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "User not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
//               // @Router       /me [delete]
// @Deprecated
func DeleteSelf(app *firebase.App, dbPool *pgxpool.Pool) http.HandlerFunc {
	return deleteUser(CALLER, app, dbPool)
}
