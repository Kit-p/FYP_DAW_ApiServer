// This file contains API middlewares for the /users/* APIs and all APIs with RBAC.

package user

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	errorhandler "daw/apiserver/pkg/handler/error"
)

type UserContextKey string

const USER UserContextKey = "user"

var ParamUserId string = "userId"

// UserContext queries the User table with id specified in the endpoint path parameter.
// The user data is then available in the context with the USER key.
func UserContext(dbPool *pgxpool.Pool) func(http.Handler) http.Handler {
	fn := func(next http.Handler) http.Handler {
		var fn http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
			// read the path parameter
			userId := chi.URLParam(req, ParamUserId)
			if userId == "" {
				render.Render(res, req,
					errorhandler.ErrInvalidRequest(fmt.Errorf("missing path parameter <%s>", ParamUserId)))
				return
			}

			// retrieve data from the database
			if user, err := dbGetUser(req.Context(), dbPool, pgx.TxOptions{}, userId); err != nil {
				render.Render(res, req, errorhandler.ErrNotFound)
				return
			} else {
				// append data to the request context
				ctx := context.WithValue(req.Context(), USER, user)
				// serve the request with the new context
				next.ServeHTTP(res, req.WithContext(ctx))
			}
		}
		return http.HandlerFunc(fn)
	}
	return fn
}

const CALLER UserContextKey = "caller"

var ParamAuthToken string = "authToken"

// RBAC stands for Role-Based Access Control, which is used for restricting access of API endpoints by the role of user.
// The user (caller) data is then available in the context with the CALLER key.
func RBAC(dbPool *pgxpool.Pool, minRole UserRole) func(http.Handler) http.Handler {
	middleware := func(next http.Handler) http.Handler {
		var fn http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
			ctx := req.Context()

			// read user ID and auth token from the Authorization header
			userId, authToken, success := req.BasicAuth()
			if !success || userId == "" || authToken == "" {
				render.Render(res, req,
					errorhandler.ErrInvalidRequest(fmt.Errorf("missing authorization header '<%s>:<%s>'[base64]", ParamUserId, ParamAuthToken)))
				return
			}

			// validate auth token of user and get user data from database
			user, err := dbGetUserWithAuth(req.Context(), dbPool, pgx.TxOptions{}, userId, authToken)
			if err != nil {
				if minRole != GUEST {
					render.Render(res, req,
						errorhandler.ErrUnauthorized(errors.New("invalid auth token")))
					return
				}

				role, enabled, createTime := GUEST, true, time.Now()
				user = &User{
					Role:       &role,
					Enabled:    &enabled,
					CreateTime: &createTime,
				}
			}
			if !*user.Enabled {
				render.Render(res, req,
					errorhandler.ErrForbidden(errors.New("account disabled")))
				return
			}

			// check access restriction
			if !user.Role.CanAccess(minRole) {
				render.Render(res, req,
					errorhandler.ErrForbidden(errors.New("insufficient privilege")))
				return
			}

			// append caller data to the request context
			ctx = context.WithValue(ctx, CALLER, user)
			// serve the request with the new context
			next.ServeHTTP(res, req.WithContext(ctx))
		}
		return http.HandlerFunc(fn)
	}
	return middleware
}
