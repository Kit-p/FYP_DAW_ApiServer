// This file contains database queries used in handlers for the /users/* APIs and all APIs with RBAC.

package user

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"

	"daw/apiserver/pkg/db"
)

var UserTableName string = "User"
var UserTokenTableName string = "User_Token"

// dbGetUsers is a package private method to query database for all web portal users.
func dbGetUsers(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions) ([]*User, error) {
	var users []*User
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT id, role, name, email, enabled, create_time FROM %s.%s",
			db.DatabaseName,
			UserTableName,
		)

		rows, err := tx.Query(context.Background(),
			sql,
		)

		for rows.Next() {
			var user *User = &User{}
			rows.Scan(&user.ID, &user.Role, &user.Name, &user.Email, &user.Enabled, &user.CreateTime)
			users = append(users, user)
		}

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return users, err
}

// dbGetUser is a package private method to query database for a web portal user with the specific userId.
func dbGetUser(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, userId string) (*User, error) {
	var user *User = &User{}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT id, role, name, email, enabled, create_time FROM %s.%s WHERE id=$1",
			db.DatabaseName,
			UserTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
			userId,
		).Scan(&user.ID, &user.Role, &user.Name, &user.Email, &user.Enabled, &user.CreateTime)

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return user, err
}

// dbGetUserWithAuth is a package private method to query database for a web portal user with the specific userId and authToken.
// If authToken is invalid or expired, no rows will be returned.
func dbGetUserWithAuth(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, userId string, authToken string) (*User, error) {
	var user *User = &User{}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"SELECT u.id, u.role, u.name, u.email, u.enabled, u.create_time FROM %s.%s u INNER JOIN %s.%s t ON u.id = t.user_id WHERE u.id=$1 AND t.token=$2 AND t.expiry_time>=$3",
			db.DatabaseName,
			UserTableName,
			db.DatabaseName,
			UserTokenTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
			userId,
			authToken,
			time.Now(),
		).Scan(&user.ID, &user.Role, &user.Name, &user.Email, &user.Enabled, &user.CreateTime)

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return user, err
}

func dbCreateUser(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, user *User) error {
	if user == nil {
		return errors.New("parameter is nil")
	}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"INSERT INTO %s.%s (id, role, name, email, enabled, create_time) VALUES ($1, $2, $3, $4, $5, $6)",
			db.DatabaseName,
			UserTableName,
		)
		_, err := tx.Exec(context.Background(),
			sql,
			user.ID,
			user.Role,
			user.Name,
			user.Email,
			user.Enabled,
			user.CreateTime,
		)
		return err
	}
	return crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
}

func dbUpdateUser(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, user *User) error {
	if user == nil {
		return errors.New("parameter is nil")
	}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"UPDATE %s.%s SET role=$2, name=$3, email=$4, enabled=$5 WHERE id=$1",
			db.DatabaseName,
			UserTableName,
		)
		_, err := tx.Exec(context.Background(),
			sql,
			user.ID,
			user.Role,
			user.Name,
			user.Email,
			user.Enabled,
		)
		return err
	}
	return crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
}

// dbDeleteUser is a package private method to delete database a web portal user with the specific userId.
func dbDeleteUser(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, userId string) error {
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"DELETE FROM %s.%s WHERE id=$1",
			db.DatabaseName,
			UserTableName,
		)
		_, err := tx.Exec(context.Background(),
			sql,
			userId,
		)
		return err
	}
	return crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
}
