// This file defines API handlers for the /issues/* APIs.

package issue

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"daw/apiserver/pkg/handler/angelbox"
	errorhandler "daw/apiserver/pkg/handler/error"

	firebase "firebase.google.com/go/v4"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	"daw/apiserver/pkg/handler/user"
	"daw/apiserver/pkg/handler/util"
)

// Handler returns a sub router that is to be mounted to the main router.
// It defines all API endpoints under /issues.
// For documentation of each handler, reference the godoc above the corresponding handler.
func Handler(app *firebase.App, dbPool *pgxpool.Pool, config *util.Config) http.Handler {
	router := chi.NewRouter()

	// make all endpoints under /issues require a minimum role of OPERATOR
	router.Use(user.RBAC(dbPool, user.OPERATOR))

	router.Get("/", RenderAllIssueGroups(dbPool))

	router.Route(fmt.Sprintf("/{%s}", ParamIssueGroupId), func(r chi.Router) {
		r.Use(IssueContext(dbPool))

		r.Get("/log", RenderIssueGroupLogs(dbPool))
		r.Get("/logs", RenderIssueGroupLogs(dbPool))
		r.Get("/", RenderIssueGroup())

		r.Put("/", UpdateIssueGroup(dbPool))
	})

	router.Route(fmt.Sprintf("/angelbox/{%s}", angelbox.ParamAngelboxId), func(r chi.Router) {
		r.Use(angelbox.AngelboxContext(dbPool))

		r.Get("/", RenderAngelboxIssueGroups(dbPool))
	})

	return router
}

// RenderAllIssueGroups godoc
// @Summary      Render all issues in the database
// @Description  Get all issues from the database and put their JSON representation into an array. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {array}   IssueGroupRes        "All the issues in JSON format"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /issue [get]
// @Router       /issues [get]
func RenderAllIssueGroups(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		issueGroups, err := dbGetAllIssueGroup(req.Context(), dbPool, pgx.TxOptions{})
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		if err := render.RenderList(res, req, NewIssueGroupListRes(issueGroups)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// RenderAngelboxIssueGroups godoc
// @Summary      Render all issues of an angelbox in the database
// @Description  Get all issues of an angelbox from the database and put their JSON representation into an array. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {array}   IssueGroupRes        "All the issues of angelboxes in JSON format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing path parameter 'angelboxId'"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "AngelBox not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /issue/angelbox/{id} [get]
// @Router       /issues/angelbox/{id} [get]
func RenderAngelboxIssueGroups(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		angelbox := req.Context().Value(angelbox.ANGELBOX).(*angelbox.Angelbox)
		if angelbox == nil {
			render.Render(res, req, errorhandler.ErrNotFound)
			return
		}

		issueGroups, err := dbGetAngelboxIssueGroup(req.Context(), dbPool, pgx.TxOptions{}, *angelbox.ID)
		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		if err := render.RenderList(res, req, NewIssueGroupListRes(issueGroups)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// RenderIssueGroup godoc
// @Summary      Render an issue
// @Description  Read an issue from the context value and render it as an JSON object. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {object}  IssueGroupRes        "The AngelBox in JSON format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing path parameter 'issueId'"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "Issue not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /issue/{id} [get]
// @Router       /issues/{id} [get]
func RenderIssueGroup() http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		issueGroup := req.Context().Value(ISSUEGROUP).(*IssueGroup)
		if issueGroup == nil {
			render.Render(res, req, errorhandler.ErrNotFound)
			return
		}

		if err := render.Render(res, req, NewIssueGroupRes(issueGroup)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// RenderIssueGroupLogs godoc
// @Summary      Render logs of an issue
// @Description  Read logs of an issue from the context value and render it as an JSON object. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true  "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Success      200            {array}   IssueRes             "The AngelBox in JSON format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing path parameter 'issueId'"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "Issue not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /issue/{id}/log [get]
// @Router       /issues/{id}/logs [get]
func RenderIssueGroupLogs(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		issueGroup := req.Context().Value(ISSUEGROUP).(*IssueGroup)
		if issueGroup == nil {
			render.Render(res, req, errorhandler.ErrNotFound)
			return
		}

		issues, err := dbGetGroupIssueLogs(req.Context(), dbPool, pgx.TxOptions{}, *issueGroup.GroupId)

		if err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}

		if err := render.RenderList(res, req, NewIssueListRes(issues)); err != nil {
			render.Render(res, req, errorhandler.ErrRender(err))
			return
		}
	}
	return http.HandlerFunc(fn)
}

// UpdateIssueGroup godoc
// @Summary      Update an issue in the database
// @Description  Update an issue in the database from an object of issue details (unchanged fields can be omitted), note: currently only <resolved> should be updated. Requires role OPERATOR or above.
// @Param        Authorization  header    string               true                                       "User ID as username and auth token as password, format '\<userId\>:\<authToken\>', in base64"
// @Param        resolved       body      boolean              true                                       "Whether the issue is resolved"
// @Success      200            {object}  IssueGroupRes        "The updated issue (with updated details)  in  JSON  format"
// @Failure      400            {object}  errorhandler.ErrRes  "Missing body parameter(s)"
// @Failure      401            {object}  errorhandler.ErrRes  "Missing '\<userId\>:\<authToken\>' in Authorization header"
// @Failure      403            {object}  errorhandler.ErrRes  "Insufficient privilege"
// @Failure      404            {object}  errorhandler.ErrRes  "Issue not found"
// @Failure      422            {object}  errorhandler.ErrRes  "Unknown error"
// @Failure      500            {object}  errorhandler.ErrRes  "Internal server error (most likely cannot connect to db / firebase)"
// {Deprecated}  @Router  /issue/{id} [put]
// @Router       /issues/{id} [put]
func UpdateIssueGroup(dbPool *pgxpool.Pool) http.HandlerFunc {
	fn := func(res http.ResponseWriter, req *http.Request) {
		ctx := req.Context()
		issueGroup := req.Context().Value(ISSUEGROUP).(*IssueGroup)
		if issueGroup == nil {
			render.Render(res, req, errorhandler.ErrNotFound)
			return
		}

		data := &IssueGroupUpdateReq{}
		if err := render.Bind(req, data); err != nil {
			render.Render(res, req, errorhandler.ErrInvalidRequest(err))
			return
		}

		issueGroup.UpdateWith(data)

		if *issueGroup.Resolved {
			resolveTime := time.Now().Local()
			issueGroup.ResolvedTime = &resolveTime
			resolvedManually := true
			issueGroup.ResolvedManually = &resolvedManually
		} else {
			issueGroup.ResolvedTime = nil
			resolvedManually := false
			issueGroup.ResolvedManually = &resolvedManually
		}

		if err := dbUpdateIssueGroup(ctx, dbPool, pgx.TxOptions{}, issueGroup); err != nil {
			render.Render(res, req, errorhandler.ErrInternal(err))
			return
		}
		render.Status(req, http.StatusCreated)
		ctx = context.WithValue(req.Context(), ISSUEGROUP, issueGroup)
		RenderIssueGroup()(res, req.WithContext(ctx))
	}
	return http.HandlerFunc(fn)
}
