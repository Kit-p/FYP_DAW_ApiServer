// This file contains API middlewares for the /issues/* APIs.

package issue

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/render"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"

	errorhandler "daw/apiserver/pkg/handler/error"
)

type IssueContextKey string

const ISSUEGROUP IssueContextKey = "issueGroup"

var ParamIssueGroupId string = "issueGroupId"

// IssueContext queries the Issue table with id specified in the endpoint path parameter.
// The issue data is then available in the context with the ISSUEGROUP key.
func IssueContext(dbPool *pgxpool.Pool) func(http.Handler) http.Handler {
	fn := func(next http.Handler) http.Handler {
		var fn http.HandlerFunc = func(res http.ResponseWriter, req *http.Request) {
			// read the path parameter
			groupId := chi.URLParam(req, ParamIssueGroupId)
			if groupId == "" {
				render.Render(res, req,
					errorhandler.ErrInvalidRequest(fmt.Errorf("missing path parameter <%s>", ParamIssueGroupId)))
				return
			}

			// retrieve data from the database
			if issueGroup, err := dbGetIssueGroup(req.Context(), dbPool, pgx.TxOptions{}, groupId); err != nil {
				render.Render(res, req, errorhandler.ErrNotFound)
				return
			} else {
				// append data to the request context
				ctx := context.WithValue(req.Context(), ISSUEGROUP, issueGroup)
				// serve the request with the new context
				next.ServeHTTP(res, req.WithContext(ctx))
			}
		}
		return http.HandlerFunc(fn)
	}
	return fn
}
