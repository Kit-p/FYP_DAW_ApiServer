// This file contains database queries used in handlers for the /issues/* APIs.

package issue

import (
	"context"
	"errors"
	"fmt"

	"github.com/cockroachdb/cockroach-go/v2/crdb/crdbpgx"
	"github.com/jackc/pgx/v4"

	"daw/apiserver/pkg/db"
)

var IssueTableName string = "Issue"

// dbGetAllIssueGroup is a package private method to query database for all grouped issues.
func dbGetAllIssueGroup(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions) ([]*IssueGroup, error) {
	var issueGroups []*IssueGroup

	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"with n as (select group_id, max(create_time) as last_occurrence, count(*) as num_occurrence from %s.%s group by group_id) select i.group_id, i.type, i.angelbox_id, i.error_code, i.detail_en, i.detail_zh, i.resolved, i.resolved_time, i.resolved_manually, n.last_occurrence, n.num_occurrence from %s.%s i inner join n on i.group_id=n.group_id and i.create_time=n.last_occurrence order by resolved asc, last_occurrence desc",
			db.DatabaseName,
			IssueTableName,
			db.DatabaseName,
			IssueTableName,
		)

		rows, err := tx.Query(context.Background(),
			sql,
		)

		for rows.Next() {
			var issueGroup *IssueGroup = &IssueGroup{}
			rows.Scan(&issueGroup.GroupId, &issueGroup.Type, &issueGroup.AngelboxId, &issueGroup.ErrorCode, &issueGroup.Detail_EN, &issueGroup.Detail_ZH, &issueGroup.Resolved, &issueGroup.ResolvedTime, &issueGroup.ResolvedManually, &issueGroup.LastOccurrence, &issueGroup.NumOccurrence)
			issueGroups = append(issueGroups, issueGroup)
		}

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return issueGroups, err
}

// dbGetAngelboxIssueGroup is a package private method to query database for all grouped issues of an AngelBox with the specific angelboxId.
func dbGetAngelboxIssueGroup(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, angelboxId string) ([]*IssueGroup, error) {
	var issueGroups []*IssueGroup

	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"with n as (select group_id, max(create_time) as last_occurrence, count(*) as num_occurrence from %s.%s where angelbox_id=$1::uuid group by group_id) select i.group_id, i.type, i.angelbox_id, i.error_code, i.detail_en, i.detail_zh, i.resolved, i.resolved_time, i.resolved_manually, n.last_occurrence, n.num_occurrence from %s.%s i inner join n on i.group_id=n.group_id and i.create_time=n.last_occurrence order by resolved asc, last_occurrence desc",
			db.DatabaseName,
			IssueTableName,
			db.DatabaseName,
			IssueTableName,
		)

		rows, err := tx.Query(context.Background(),
			sql,
			angelboxId,
		)

		for rows.Next() {
			var issueGroup *IssueGroup = &IssueGroup{}
			rows.Scan(&issueGroup.GroupId, &issueGroup.Type, &issueGroup.AngelboxId, &issueGroup.ErrorCode, &issueGroup.Detail_EN, &issueGroup.Detail_ZH, &issueGroup.Resolved, &issueGroup.ResolvedTime, &issueGroup.ResolvedManually, &issueGroup.LastOccurrence, &issueGroup.NumOccurrence)
			issueGroups = append(issueGroups, issueGroup)
		}

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return issueGroups, err
}

// dbGetIssueGroup is a package private method to query database for a grouped issue with the specific groupId.
func dbGetIssueGroup(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, groupId string) (*IssueGroup, error) {
	var issueGroup *IssueGroup = &IssueGroup{}

	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"with n as (select group_id, max(create_time) as last_occurrence, count(*) as num_occurrence from %s.%s where group_id=$1::uuid group by group_id) select i.group_id, i.type, i.angelbox_id, i.error_code, i.detail_en, i.detail_zh, i.resolved, i.resolved_time, i.resolved_manually, n.last_occurrence, n.num_occurrence from %s.%s i inner join n on i.group_id=n.group_id and i.create_time=n.last_occurrence",
			db.DatabaseName,
			IssueTableName,
			db.DatabaseName,
			IssueTableName,
		)

		err := tx.QueryRow(context.Background(),
			sql,
			groupId,
		).Scan(&issueGroup.GroupId, &issueGroup.Type, &issueGroup.AngelboxId, &issueGroup.ErrorCode, &issueGroup.Detail_EN, &issueGroup.Detail_ZH, &issueGroup.Resolved, &issueGroup.ResolvedTime, &issueGroup.ResolvedManually, &issueGroup.LastOccurrence, &issueGroup.NumOccurrence)

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return issueGroup, err
}

// dbGetGroupIssueLogs is a package private method to query database for all issue logs of a grouped issue with the specific groupId.
func dbGetGroupIssueLogs(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, groupId string) ([]*Issue, error) {
	var issues []*Issue

	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"select id, group_id, type, heartbeat_id, angelbox_id, error_code, detail_en, detail_zh, resolved, resolved_time, resolved_manually, create_time from %s.%s where group_id=$1 order by create_time desc",
			db.DatabaseName,
			IssueTableName,
		)

		rows, err := tx.Query(context.Background(),
			sql,
			groupId,
		)

		for rows.Next() {
			var issue *Issue = &Issue{}
			rows.Scan(&issue.ID, &issue.GroupId, &issue.Type, &issue.HeartbeatId, &issue.AngelboxId, &issue.ErrorCode, &issue.Detail_EN, &issue.Detail_ZH, &issue.Resolved, &issue.ResolveTime, &issue.ResolvedManually, &issue.CreateTime)
			issues = append(issues, issue)
		}

		return err
	}
	err := crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
	return issues, err
}

// dbUpdateIssueGroup is a package private method to update database an issue group using data in issueGroup.
func dbUpdateIssueGroup(ctx context.Context, conn crdbpgx.Conn, txOptions pgx.TxOptions, issueGroup *IssueGroup) error {
	if issueGroup == nil {
		return errors.New("parameter is nil")
	}
	fn := func(tx pgx.Tx) error {
		sql := fmt.Sprintf(
			"UPDATE %s.%s set resolved=$2, resolved_time=$3, resolved_manually=$4 where group_id=$1",
			db.DatabaseName,
			IssueTableName,
		)
		_, err := tx.Exec(context.Background(),
			sql,
			issueGroup.GroupId,
			issueGroup.Resolved,
			issueGroup.ResolvedTime,
			issueGroup.ResolvedManually,
		)
		return err
	}
	return crdbpgx.ExecuteTx(ctx, conn, txOptions, fn)
}
