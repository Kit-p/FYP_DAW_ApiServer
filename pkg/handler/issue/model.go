// This file contains types and corresponding JSON schemas used in /issues/* APIs.

package issue

import (
	"errors"
	"net/http"
	"time"

	"github.com/go-chi/render"
)

type IssueType string

// IssueType constants
const (
	ANGELBOX IssueType = "angelbox" // AngelBox related
	SERVER   IssueType = "server"   // server related (unused)
	UNKNOWN  IssueType = "unknown"  // unknown (unused)
)

// IssueGroup example
type IssueGroup struct {
	GroupId          *string    `json:"group_id"`
	Type             *IssueType `json:"type"`
	AngelboxId       *string    `json:"angelbox_id"`
	ErrorCode        *int16     `json:"error_code"`
	Detail_EN        *string    `json:"detail_en"`
	Detail_ZH        *string    `json:"detail_zh"`
	Resolved         *bool      `json:"resolved"`
	ResolvedTime     *time.Time `json:"resolved_time"`
	ResolvedManually *bool      `json:"resolved_manually"`
	LastOccurrence   *time.Time `json:"last_occurrence"`
	NumOccurrence    *int       `json:"num_occurrence"`
}

// UpdateWith updates the issue group with a possibly incomplete issue object updateRequest.
// Validations and conditions are checked before updating the values.
func (issueGroup *IssueGroup) UpdateWith(updateRequest *IssueGroupUpdateReq) error {
	if updateRequest == nil {
		return errors.New("parameter is nil")
	}
	if updateRequest.Resolved == nil {
		return errors.New("missing body parameter: <resolved>")
	} else {
		issueGroup.Resolved = updateRequest.Resolved
	}

	return nil
}

// IssueGroupUpdateReq example
type IssueGroupUpdateReq struct {
	Resolved *bool `json:"resolved"`
}

// Bind defines the pre-processing of JSON request body before converting to IssueGroupUpdateReq.
func (body *IssueGroupUpdateReq) Bind(req *http.Request) error {
	if req.Method == http.MethodPut {
		if body.Resolved == nil {
			return errors.New("missing body parameter: <resolved>")
		}
	}
	return nil
}

// IssueGroupRes example
type IssueGroupRes struct {
	*IssueGroup
}

// Render defines the post-processing of IssueGroupRes before converting to JSON response body.
func (body *IssueGroupRes) Render(res http.ResponseWriter, req *http.Request) error {
	return nil
}

// NewIssueGroupRes is the factory method for IssueGroupRes.
func NewIssueGroupRes(issueGroup *IssueGroup) *IssueGroupRes {
	return &IssueGroupRes{IssueGroup: issueGroup}
}

// NewIssueGroupListRes is the factory method for an array of IssueGroupRes.
func NewIssueGroupListRes(issueGroups []*IssueGroup) []render.Renderer {
	list := []render.Renderer{}
	for _, issueGroup := range issueGroups {
		list = append(list, NewIssueGroupRes(issueGroup))
	}
	return list
}

// Issue example
type Issue struct {
	ID               *string    `json:"id"`
	GroupId          *string    `json:"group_id"`
	Type             *IssueType `json:"type"`
	HeartbeatId      *string    `json:"heartbeat_id"`
	AngelboxId       *string    `json:"angelbox_id"`
	ErrorCode        *int16     `json:"error_code"`
	Detail_EN        *string    `json:"detail_en"`
	Detail_ZH        *string    `json:"detail_zh"`
	Resolved         *bool      `json:"resolved"`
	ResolveTime      *time.Time `json:"resolved_time"`
	ResolvedManually *bool      `json:"resolved_manually"`
	CreateTime       *time.Time `json:"create_time"`
}

// IssueRes example
type IssueRes struct {
	*Issue
}

// Render defines the post-processing of IssueRes before converting to JSON response body.
func (body *IssueRes) Render(res http.ResponseWriter, req *http.Request) error {
	return nil
}

// NewIssueRes is the factory method for IssueRes.
func NewIssueRes(issue *Issue) *IssueRes {
	return &IssueRes{Issue: issue}
}

// NewIssueListRes is the factory method for an array of IssueRes.
func NewIssueListRes(issues []*Issue) []render.Renderer {
	list := []render.Renderer{}
	for _, issue := range issues {
		list = append(list, NewIssueRes(issue))
	}
	return list
}
