// This file contains functions needed to initialize a database connection.

package db

import (
	"context"
	"math"
	"time"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"go.uber.org/zap"

	"daw/apiserver/pkg/handler/util"
)

// name of the database, should be updated together with the database script in "DAW_DevOps" when needed
var DatabaseName string = "daw"

// InitConnection uses optimal settings to connect to the database at dbHost.
func InitConnection(logger *zap.Logger, dbHost string, config *util.Config) *pgxpool.Pool {
	dbConfig, err := pgxpool.ParseConfig(dbHost)
	if err != nil {
		logger.Fatal("error configuring the database connection",
			zap.Error(err),
			zap.String("connString", dbHost),
		)
	}

	// need to keep at least 1 connection open so latency remains low after long idle time
	dbConfig.MinConns = 1
	// need to maximize the connection lifetime so they do not get closed after long idle time
	dbConfig.MaxConnLifetime = time.Duration(math.MaxInt64)
	dbConfig.ConnConfig.Database = DatabaseName
	// set log level to Info for production
	dbConfig.ConnConfig.LogLevel = pgx.LogLevelInfo
	if config.Environment == util.DEVELOPMENT {
		// set log level to Debug for development
		dbConfig.ConnConfig.LogLevel = pgx.LogLevelDebug
	}
	// connect to the database with the custom configuration
	pool, err := pgxpool.ConnectConfig(context.Background(), dbConfig)
	if err != nil {
		logger.Fatal("error connecting to the database",
			zap.Error(err),
		)
	}
	logger.Info("successfully connected to the database",
		zap.String("host", dbConfig.ConnConfig.Host),
		zap.Uint16("port", dbConfig.ConnConfig.Port),
		zap.String("database", dbConfig.ConnConfig.Database),
	)
	// return the connection pool instance
	return pool
}
