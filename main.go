// This file is the entrypoint of the API server.

package main

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	firebase "firebase.google.com/go/v4"
	"go.uber.org/zap"
	"google.golang.org/api/option"

	"daw/apiserver/pkg/backendworker"
	"daw/apiserver/pkg/db"
	"daw/apiserver/pkg/handler"
	"daw/apiserver/pkg/handler/util"
)

// package-level global variables, can be overridden with environment variables
var (
	// path to configuration file for the API server
	config_filepath string = util.GetEnv("CONFIG_FILEPATH", "")
)

// main is automatically executed when the program starts.
// It initializes all modules and starts them.
// It keeps the program alive until receiving terminating signals and attempts to gracefully exit by then.
func main() {
	// create a goroutine wait group
	var wg sync.WaitGroup

	// initialize the zap logger
	var logger *zap.Logger
	// use the production preset (logs level Info or above to stderr)
	logger, _ = zap.NewProduction()
	// deferred: flush logger buffer when the main() function returns
	defer logger.Sync()

	// read config file
	if config_filepath == "" {
		logger.Fatal("missing configuration file, please specify the environment variable <CONFIG_FILEPATH>")
	}
	config, err := util.ParseConfig(config_filepath)
	if err != nil {
		logger.Fatal("error loading configuration file",
			zap.Error(err),
			zap.String("CONFIG_FILEPATH", config_filepath),
		)
	}

	// use the development preset if config specifies development environment (panic level Panic instead of logs)
	if config.Environment == util.DEVELOPMENT {
		logger = logger.WithOptions(zap.Development())
	}

	// initialize database connection
	dbPool := db.InitConnection(logger, config.DatabaseConnectionString, config)
	// deferred: close the connection when the main() function returns
	defer dbPool.Close()

	// initialize backend worker and start in background
	backendWorker, err := backendworker.NewBackendWorker(logger, dbPool, config.AppServerAPIEndpoint, config.MqttBrokerAddress, backendworker.NewTLSConfig(logger))
	if err != nil {
		logger.Fatal("error starting mqtt client",
			zap.Error(err),
		)
	} else {
		// log the MQTT client details
		opts := backendWorker.MqttClient.OptionsReader()
		logger.Info("successfully started mqtt client",
			zap.String("broker", opts.Servers()[0].Host),
			zap.String("username", opts.Username()),
			zap.String("clientID", opts.ClientID()),
		)
	}

	// initialize firebase admin sdk
	firebaseOpt := option.WithCredentialsFile(config.FirebaseServiceAccountCredentialsPath)
	firebaseApp, err := firebase.NewApp(context.Background(), nil, firebaseOpt)
	if err != nil {
		logger.Fatal("error initializing firebase admin sdk",
			zap.Error(err),
		)
	}

	// initialize the http server
	server := &http.Server{Addr: config.ListenAddress, Handler: handler.Handler(firebaseApp, dbPool, config, backendWorker)}

	// declare contexts used to shutdown the http server
	serverCtx, serverCancel := context.WithCancel(context.Background())

	// specify signals to listen such that the program can gracefully terminate
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	// start the http server and wait until it stops
	wg.Add(1)
	go func() {
		// deferred: notify the wait group when this goroutine returns
		defer wg.Done()

		// start the http listener and start serving content
		err := server.ListenAndServe()
		if err == http.ErrServerClosed {
			logger.Info("api server has been gracefully shut down")
		} else if err != nil {
			logger.Fatal("error starting api server",
				zap.Error(err),
				zap.String("url", server.Addr),
			)
		}

		// listen to server shutdown events
		<-serverCtx.Done()
		if err := serverCtx.Err(); err != nil && err != context.Canceled {
			logger.Fatal("error stopping api server",
				zap.Error(err),
				zap.String("url", server.Addr),
			)
		}
	}()

	logger.Info("successfully started up api server",
		zap.String("url", server.Addr),
	)

	// listen to the signals specified
	<-sig

	// attempt to gracefully shutdown the application within the specific timeout
	shutdownTimeout := 5 * time.Second

	// attempt to gracefully shutdown the http server
	logger.Warn("attempting to gracefully shut down api server",
		zap.Duration("timeout", shutdownTimeout),
	)
	// declare contexts used to stop the shutdown process
	shutdownCtx, shutdownCancel := context.WithTimeout(serverCtx, shutdownTimeout)

	// detect graceful shutdown timeout and forcefully exit
	wg.Add(1)
	go func() {
		// deferred: notify the wait group when this goroutine returns
		defer wg.Done()

		// listen to shutdown completion events
		<-shutdownCtx.Done()
		if shutdownCtx.Err() == context.DeadlineExceeded {
			logger.Fatal("timeout shutting down api server, now forcefully exit",
				zap.Duration("timeout", shutdownTimeout),
			)
		}
	}()

	// shutdown the http server
	if err := server.Shutdown(shutdownCtx); err != nil {
		logger.Fatal("error shutting down api server",
			zap.Error(err),
		)
	}

	// stop the mqtt client
	if err := backendWorker.Stop(); err != nil {
		logger.Error("error shutting down mqtt client",
			zap.Error(err),
		)
	} else {
		logger.Info("mqtt client has been gracefully shutdown")
	}

	// clean up the contexts to avoid leaking
	serverCancel()
	shutdownCancel()

	// wait for all the goroutines to return
	wg.Wait()
}
